#/usr/bin/python

# GNU Affero General Public License v3.0
#
# CGAR (Clinical Genome & Ancestry Report) - an interactive platform to identify
# phenotype-associated variants from next-generation sequencing data
# Copyright (C) 2017-2020  In-Hee Lee, Jose A Negron La Rosa
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
import sys
import urllib, json
import requests
import time
import datetime
import re
import unicodedata

from classes.myFindZebra import myFindZebra
#from ancestry import get_ancestry

from flask import Flask, flash, render_template, request, url_for, redirect, session
from flask_mysqldb import MySQL

mysql = MySQL()
app = Flask(__name__, static_url_path="", static_folder="static")
app.config.from_object('config')

app.jinja_env.add_extension('jinja2.ext.do')

#for session security
#app.secret_key = key

def my_format_time(dt):
	return(dt.strftime("%b. %d, %Y|%I:%M:%S %p EST"))
#	return(datetime.datetime.strptime(inStr, '%Y-%m-%d %H:%M:%S').strftime("%Y-%b-%d %I:%M:%S %p EST"))
app.jinja_env.globals.update(my_format_time=my_format_time)
app.jinja_env.auto_reload = True

# MySQL db connection configurations
mysql.init_app(app)

@app.before_request
def before_request():
	session.permenant = True
	app.permanent_session_lifetime = datetime.timedelta(minutes=30)

@app.route('/redirect')
def test():
    if session.get('username'):
#         cur = connect_admin_db()
#         stmt = "SELECT userID FROM session WHERE sessionID = '%s'" % (session_id)
#         cur.execute(stmt)
#         user_id = cur.fetchone()
#         session['username'] = user_id[0] 
         return redirect(url_for("genome_select"))
#         return redirect(url_for(next_page)+sid)
    else:
         return render_template("login.html")
     
@app.route('/login')
def login():
    return render_template("login.html")

@app.route('/guest')
def guest():
    session['username'] = "guest"
    return redirect(url_for("genome_select"))

@app.route('/err')
def err():
    return render_template("fixing.html")

@app.route('/')
def index():
    if not session.get('username'):
        session['username'] = 'guest'
        session['priviledge'] = 'no'
#    session_id = request.cookies.get('CGISESSID')
#    if session_id:
#         cur = mysql.connection.cursor()
#         stmt = "SELECT userID FROM session WHERE sessionID = '%s'" % (session_id)
#         cur.execute(stmt)
#         user_id = cur.fetchone()
#         session['username'] = user_id[0]

    return render_template("intro.html")
#    return render_template("fixing.html")

@app.route('/helpfaq')
def helpfaq():
    return render_template("help.html")


@app.route('/logout')
def logout():
    session.pop('username', None)
#    session_id = request.cookies.get('CGISESSID')
#    session_id.set_cookie('CGISESSID', '', expires=0)
    return redirect(url_for('index'))

@app.route('/version_info')
def version_info():
    return render_template("version_info.html")

@app.route('/user_auth', methods=['POST'])
def check_user():
    username = request.form['username']
    pwd = request.form['pwd']
    cur = connect_admin_db()
    cur.execute('''SELECT DISTINCT user_ID FROM user_account''')
    idL = map(lambda x: x[0], cur.fetchall())
    if username not in idL:
	 pw = 'wrong'
         return render_template("login.html",pw=pw)
#        return redirect(url_for('login'))
    cur.execute("SELECT count(*) FROM user_account WHERE user_ID = '%s' and password = SHA2('%s', 512)" % (username, pwd))
    rv = cur.fetchone()
    if rv[0] != 1:
        return redirect(url_for('login'), pw='wrong')
    else: 
        session['username'] = username
	cur.execute("SELECT privileged FROM user_account WHERE user_ID = '%s'" % (username))
	rv = cur.fetchone()
        if rv[0] == '\x01':
	    priviledge = 'yes'
#            session['username'] = 'reviewer'
	else:
	    priviledge = 'no'
	session['priviledge'] = priviledge
	return redirect(url_for("genome_select"))

@app.route('/upload_genome', methods=['GET','POST'])
def upload_genome():
    cur = connect_admin_db()
    if not session.get('username'):
	session['username'] = "guest"
        session['priviledge'] = 'no'
    else:
        cur.execute("SELECT privileged FROM user_account WHERE user_ID = '%s'" % session['username'])
        rv = cur.fetchone()
        if rv[0] == '\x01':
            priviledge = 'yes'
        else:
            priviledge = 'no'
        session['priviledge'] = priviledge

    priviledge = session['priviledge']

    username = session['username']
    stmt = "SELECT genome_label, filename, genome_build, genome_ancestry, annotation, uploaded_at FROM genome_info WHERE userID = '%s'" % username
    cur.execute(stmt)
    genomeL = cur.fetchall()

    if 'FILE' in request.files:
        genome_build = request.form['genome_build']
        genome_ancestry = request.form['genome_ancestry']
        upfile = request.files['FILE']
        if upfile.filename == '':
            return render_template('upload_report.html', success=False, reason='no_file', genomeL=genomeL)
        else:
            filename = upfile.filename
            if filename[-4:].lower() == '.vcf' or filename[-7:].lower() == '.vcf.gz':
                fullname = os.path.join(os.environ['CGAR_HOME'], 'user_accounts/%s/%s' % (username, filename))
                upfile.save( fullname )

                ## get sample IDs in VCF
                idL = []
                if filename[-4:].lower() == '.vcf':
                    idL = os.popen('grep -m 1 CHROM %s | cut -f 10-' % fullname).readline().strip().split('\t')
                else:
                    idL = os.popen('zcat %s | grep -m 1 CHROM | cut -f 10-' % fullname).readline().strip().split('\t')
                for id in idL:
                    if not re.match('[A-Za-z0-9_]+', id.strip()):
                        return render_template('upload_report.html', success=False, reason='bad_sample_id', genomeL=genomeL)

                cur.execute("SELECT REPLACE(UUID(), '-', '_')")
                uuid = cur.fetchone()[0]
                stmt = "INSERT INTO genome_info VALUES ('%s', '(%s sample(s))', '%s', '%s', '%s', 'none', '%s', NULL, NULL, NOW())" % (uuid, len(idL), genome_build, genome_ancestry, username, filename)
                cur.execute(stmt)
                mysql.connection.commit()
                annotate_genome(username, uuid, filename, genome_build, genome_ancestry)

                stmt = "SELECT genome_label, filename, genome_build, genome_ancestry, annotation, uploaded_at FROM genome_info WHERE userID = '%s'" % username
                cur.execute(stmt)
                genomeL = cur.fetchall()

                return render_template('upload_report.html', success=True, filename=filename, genomeL=genomeL)
            else:
                return render_template('upload_report.html', success=False, reason='bad_file', genomeL=genomeL)
    else:
        return render_template('upload_report.html', success=False, reason='new', genomeL=genomeL)

def annotate_genome(username, file_uuid, input_filename, build, ancestry):
    timestamp = datetime.datetime.now().strftime('%Y_%m_%d_%H_%M_%S')
    cmd_base = os.environ['CGAR_HOME']
    user_account_dir = cmd_base + '/user_accounts/' + username
    pipeline = os.path.join(cmd_base, 'report_pipeline.sh')
    load_data = os.path.join(cmd_base, 'vep2table.py')

    if input_filename[-4:].lower() == '.vcf':
        output_filename = input_filename[:-4] + '.vep.vcf.gz'
    else:
        output_filename = input_filename[:-7] + '.vep.vcf.gz'

    script_name = "%s.%s.GA.sh" % (timestamp, file_uuid)
    script_name_full = os.path.join(user_account_dir, script_name)

    script = open(script_name_full, 'w')
    script.write("#!/bin/bash\n")
    script.write("cd %s\n" % os.environ['CGAR_HOME'])
    script.write("bash %s /work/user_accounts/%s/%s /work/user_accounts/%s/%s %s\n" % (pipeline, username,input_filename, username,output_filename, build))
    script.write("python %s %s/user_accounts/%s/%s %s %s %s %s\n" % (load_data, os.environ['CGAR_HOME'],username,output_filename, file_uuid, build, ancestry, username))
    script.flush()
    script.close()
    cur = connect_admin_db()
    cur.execute("INSERT INTO job_queue VALUES ('%s', '%s', '%s')" % (timestamp, username, script_name_full))
    mysql.connection.commit()

@app.route('/genome_select')
def genome_select():
    cur = connect_admin_db()
    if not session.get('username'):
        session['username'] = "guest"
        session['priviledge'] = 'no'
    else:
        cur.execute("SELECT privileged FROM user_account WHERE user_ID = '%s'" % (session['username']))
        rv = cur.fetchone()
        if rv[0] == '\x01':
            priviledge = 'yes'
        else:
            priviledge = 'no'
        session['priviledge'] = priviledge

    genomeL = list_genomes()
    return render_template('report_params.html', genomes=genomeL)

def connect_admin_db():
    #con = mysql.connection.connect( host=app.config['MYSQL_HOST'], user=app.config['MYSQL_USER'], password=app.config['MYSQL_PASSWORD'], database='CGAR_web_admin')
    app.config['MYSQL_DB'] = os.environ['CGAR_DB_ADMIN']
    return mysql.connection.cursor()

def connect_user_db(): #helper method: returns mysql cursor for 'web user' db
    app.config['MYSQL_DB'] = os.environ['CGAR_DB_USER']
    return mysql.connection.cursor()

def list_genomes(): # list of genomes + uuids for current user + guest samples
    cur = connect_admin_db()
    stmt = "SELECT distinct genome_label,genome_ID,userID FROM genome_info WHERE annotation <> 'none' AND (userID = '%s' or userID = 'guest') ORDER BY genome_label" % (session['username'])
    cur.execute(stmt)
    rv = cur.fetchall()
    genomeL = []
    for row in rv:
        (genome_label, genome_ID, user_ID) = row
        if session['username'] == 'guest':
            genomeL.append( row )
        else:
            if user_ID != 'guest':
                genomeL.append( row )
            elif user_ID == 'guest' and genome_label in ['trio1_NA12878_daughter','trio1_NA12891_father','trio1_NA12892_mother','trio2_GM24631_son','trio2_GM24694_father','trio2_GM24695_mother','trio3_NA24143_mother','trio3_NA24149_father','trio3_NA24385_son','Miller','schinzel_giedion','patient1_cancer_melanoma','patient1_blood_melanoma']:
                genomeL.append( row )
    return(genomeL)
    
@app.route('/report')
def generate_report():
    ts = time.time()
    begin_time = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S %f')

    session_id = request.cookies.get('CGISESSID')

    cur = connect_admin_db()
    if session_id:
        stmt = "SELECT userID FROM session WHERE sessionID = '%s'" % (session_id)
        cur.execute(stmt)
        user_id = cur.fetchone()
        session['username'] = user_id[0]
	username = session['username']

    if not session.get('username'):
#	username = session['guest']
	username = 'guest'
	session['username'] = 'guest'
	priviledge = 'no'
    else:
        cur.execute("SELECT privileged FROM user_account WHERE user_ID = '%s'" % (session['username']))
        rv = cur.fetchone()
        if rv[0] == '\x01':
            priviledge = 'yes'
        else:
            priviledge = 'no'
        session['priviledge'] = priviledge

#        priviledge = session['priviledge']

    rp1 = list_genomes()
#--------
#    genome_table_name = (session['username'] + "_genome_info")
#
#    stmt = "SELECT distinct genome_label FROM gNOME_web_user.%s WHERE analysis <> 'none' ORDER BY genome_label" % (genome_table_name)
#    cur = mysql.connection.cursor()
#    cur.execute(stmt)
#    rp1 = cur.fetchall()
##    rp1 += ((u'trio1_NA12878_daughter',), (u'trio1_NA12891_father',), (u'trio1_NA12892_mother',), (u'trio2_GM24631_son',), (u'trio2_GM24694_father',), (u'trio2_GM24695_mother',), (u'trio3_NA24143_mother',), (u'trio3_NA24149_father',), (u'trio3_NA24385_son',), (u'Miller',), (u'schinzel_giedion',), (u'patient1_cancer_melanoma',), (u'patient1_blood_melanoma',))
#    rp1 = ((u'trio1_NA12878_daughter',), (u'trio1_NA12891_father',), (u'trio1_NA12892_mother',), (u'trio2_GM24631_son',), (u'trio2_GM24694_father',), (u'trio2_GM24695_mother',), (u'trio3_NA24143_mother',), (u'trio3_NA24149_father',), (u'trio3_NA24385_son',), (u'Miller',), (u'schinzel_giedion',), (u'patient1_cancer_melanoma',), (u'patient1_blood_melanoma',))
#
    raw_genome_ID = request.args.get('select_genome')
#    sample_genomes = ['trio1_NA12878_daughter','trio1_NA12891_father','trio1_NA12892_mother','trio2_GM24631_son','trio2_GM24694_father','trio2_GM24695_mother','trio3_NA24143_mother','trio3_NA24149_father','trio3_NA24385_son','Miller','schinzel_giedion','patient1_cancer_melanoma','patient1_blood_melanoma']
#    if raw_genome in sample_genomes:
##	    genome_analysis_table = "guest_" + raw_genome + "_MA_v1_0_cds"
#	    genome_analysis_table = "test.guest_" + raw_genome + "_MA_v1_0_cds"
#            genome_table_name = ("guest_genome_info")
#    else:
#            genome_analysis_table = session['username'] + "_" + raw_genome + "_MA_v1_0_cds"
#------ 
    genome_analysis_table = os.environ['CGAR_DB_USER'] + '.' + raw_genome_ID
    cur = connect_admin_db()
    stmt = "SELECT genome_label, genome_build, genome_ancestry FROM genome_info WHERE genome_ID = '%s'" % (raw_genome_ID)
    cur.execute(stmt)
    ( raw_genome, genome_build, upload_anc ) = cur.fetchone()
    ## get genome build : hg19 or hg18

    cur = connect_user_db()
    stmt = "SELECT AFR,AMR,EAS,EUR,SAS FROM %s.genome_ancestry WHERE genome_ID = '%s'" % (os.environ['CGAR_DB_USER'], raw_genome_ID)
    cur.execute(stmt)
    vs = cur.fetchone()
    anc = {'afr':vs[0], 'amr':vs[1], 'eas':vs[2], 'eur':vs[3], 'sas':vs[4]}

    if upload_anc == 'AFR' or upload_anc == 'AMR' or upload_anc == 'EAS' or upload_anc == 'EUR' or upload_anc == 'SAS':
	upload_anc = upload_anc.lower()
    else:
	upload_anc = 'none'

    pred_anc = ""
    max_anc = 0
    for a in anc.keys():
        if max_anc < anc[a]:
            max_anc = anc[a]
            pred_anc = a
    main_anc = []
    for i in vs:
        main_anc.append(i)
    father_anc = request.args.get('father')
    mother_anc = request.args.get('mother')

    if mother_anc != 'no_mother' and father_anc != 'no_father' :
        stmt = "SELECT AFR,AMR,EAS,EUR,SAS FROM %s.genome_ancestry WHERE genome_ID = '%s'" % (os.environ['CGAR_DB_USER'],mother_anc)
        cur.execute(stmt)
        vs = cur.fetchone()
        mother_anc = []
        for i in vs:
            mother_anc.append(i)
        stmt = "SELECT AFR,AMR,EAS,EUR,SAS FROM %s.genome_ancestry WHERE genome_ID = '%s'" % (os.environ['CGAR_DB_USER'],father_anc)
        cur.execute(stmt)
        vs = cur.fetchone()
        father_anc = []
        for i in vs:
            father_anc.append(i)

    else:
	mother_anc = 'no_mother'
	father_anc = 'no_father'

    df_anc = request.args.get('df_anc')
    gt_anc = request.args.get('gt_anc')
    sr_anc = request.args.get('sr_anc')

    orpdic = open('%s/orphanet_dictionary.txt' % os.environ['CGAR_HOME'], 'r')
    orpdic2 = {}
    for orp in orpdic:
	orp = orp.strip()
	orp = orp.split('\t')
	orp[1] = orp[1].decode('iso-8859-1').encode('ascii','ignore')
	if orp[0] not in orpdic2:
		orpdic2[orp[0]] = ["\t".join(orp)] 
	else:
		orpdic2[orp[0]].append("\t".join(orp))

    ts = time.time()
    anc_time = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S %f')

        ##    ##   #########      ###   ###        #######
        ##    ##   ##            ## ## ## ##       ##    ## 
        ########   ##  #####    ##   ###   ##      ##     ##
        ##    ##   ##     ##   ##           ##     ##    ##   
        ##    ##   #########  ##             ##    #######

    h_zyg = request.args.get('h_zyg')
    h_hom = request.args.get('h_hom')
    h_het = request.args.get('h_het')

    zyg = zygosity(h_zyg,h_hom,h_het)
    zygo = zyg[0]
    h_zygo = zyg[1]

    i_all = request.args.get('h_All')
    i_hig = request.args.get('h_hig')
    i_mod = request.args.get('h_mod')
    i_low = request.args.get('h_low')
    i_abl = request.args.get('h_ablation')
    i_acc = request.args.get('h_acceptor')
    i_don = request.args.get('h_donor')
    i_stg = request.args.get('h_stop-gained')
    i_fra = request.args.get('h_frameshift')
    i_stol = request.args.get('h_stop-lost')
    i_stal = request.args.get('h_start-lost')
    i_amp = request.args.get('h_amplification')
    i_ins = request.args.get('h_insertion')
    i_del = request.args.get('h_deletion')
    i_mis = request.args.get('h_missense')
    i_pro = request.args.get('h_prot-altering')
    i_reg = request.args.get('h_reg-region')
    i_spl = request.args.get('h_splice-region')
    i_inc = request.args.get('h_incomplete')
    i_sta = request.args.get('h_start-retained')
    i_sto = request.args.get('h_stop-retained')
    i_syn = request.args.get('h_synonymous')
    h_impact_listV2 = all_impactV2(i_all,i_hig,i_mod,i_low,i_abl,i_acc,i_don,i_stg,i_fra,i_stol,i_stal,i_amp,i_ins,i_del,i_mis,i_pro,i_reg,i_spl,i_inc,i_sta,i_sto,i_syn)
    h_impact3 = h_impact_listV2[0]
    h_impact2 = h_impact_listV2[1]

    h_ancestry = request.args.get('h_ancestry')
    if gt_anc == 'gt_anc':
	h_ancestry = pred_anc
    elif sr_anc == 'sr_anc' and upload_anc != 'none':
	h_ancestry = upload_anc
    h_ancestry_q = h_ancestry
    h_allele_f = request.args.get('h_af')
    h_allele_f2 = h_allele_f
    h_allele_q = h_allele_f
    h_af_anc = af_anc(h_allele_f,h_ancestry)

    dmOnly = request.args.get('dmOnly')
    dmQue = request.args.get('dmQue')
    dmOther = request.args.get('dmOther')

    cv = ""
    count5 = 0  
    if dmOnly ==  "dmOnly":
	cv = 'DM'
        count5 += 1
    if dmQue == "dmQue" and cv == "":
	cv = 'DM?'
    elif dmQue =="dmQue":
	cv += ' + DM?'
	count5 += 1
    if dmOther == "dmOther" and cv == "":
	cv = "Other"
    elif dmOther == "dmOther":
	cv += " + Other"
        count5 += 1
    if cv == "":
	cv = "DM"
    
    CV = " "
    if count5 == 3:
	CV = " "
    elif dmOnly == "dmOnly" and dmQue == "dmQue" or cv == "DM + DM?":
	CV = " AND HGMD_class like '%DM%'" 
    elif dmOnly == "dmOnly" and dmOther == "dmOther":
	CV = " AND HGMD_class != 'DM?'"
    elif dmQue == "dmQue" and dmOther == "dmOther":
	CV = " AND HGMD_class != 'DM'"
    elif dmOnly == "dmOnly":
	CV = " And HGMD_class = 'DM'"
    elif dmQue == "dmQue":
        CV = " And HGMD_class = 'DM?'"
    elif dmOther == "dmOther":
        CV = " And HGMD_class != 'DM' and HGMD_class != 'DM?'"

    stmt=("SELECT * from %s as A where A.Symbol != '' AND A.Pick = 1 AND HGMD_class != '' %s %s %s %s" % (genome_analysis_table, h_af_anc, h_impact3, CV, zygo))
    sys.stderr.write("[LOG] HGMD: %s\n" % stmt)
    sys.stderr.flush()
    dic1 = dictsV2(stmt,h_ancestry,'hgmd'," "," ")

    ts = time.time()
    hgmd_time = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S %f')
    sys.stderr.write("[LOG] HGMD time: %s\n" % hgmd_time)
    sys.stderr.flush()

        #########      ###            ####   ###
        ###            ###            ### #  ###
        ###            ###            ### ## ### 
        ###            ###            ###  # ###
        #########      #########      ###   ####

    c_zyg = request.args.get('c_zyg')
    c_hom = request.args.get('c_hom')
    c_het = request.args.get('c_het')

    zyg = zygosity(c_zyg,c_hom,c_het)
    zygo = zyg[0]
    c_zygo = zyg[1]

    if 'genotype' in zygo:
        zygo = zygo.replace('genotype','A.genotype')

    cpath = request.args.get('path')
    clpath = request.args.get('lpath')
    cuncsig = request.args.get('uncsig')
    clbeni = request.args.get('lbeni')
    cbeni = request.args.get('beni')

    pathogenic = ''
    cv2 = ""
    countP = 0
    if cpath ==  "path":
        cv2 = 'Pathogenic'
        countP += 1
    if clpath == "lpath" and cv2 == "":
        cv2 = 'Likely pathogenic'
    elif clpath =="lpath":
        cv2 += ' + likely pathogenic'
        countP += 1
    if cuncsig == "uncsig" and cv2 == "":
        cv2 = 'Uncertain significance'
    elif cuncsig =="uncsig":
        cv2 += ' + uncertain significance'
        countP += 1
    if clbeni == "lbeni" and cv2 == "":
        cv2 = 'Likely benign'
    elif clbeni =="lbeni":
        cv2 += ' + likely benign'
        countP += 1
    if cbeni == "beni" and cv2 == "":
        cv2 = 'Benign'
    elif cbeni =="beni":
        cv2 += ' + benign'
        countP += 1
    if cv2 == "":
        cv2 = "Pathogenic + likely pathogenic"

    CV = " "
    if countP == 5:
        CV = " AND ( B.clin_sig like '%Pathogenic%' or B.clin_sig like '%Likely_pathogenic%' or B.clin_sig like '%Uncertain_significance%' or B.clin_sig like '%Likely_benign%' or B.clin_sig like '%Benign%') and (B.clin_sig not like '%Conflict%') " 
    elif cv2 == 'Pathogenic + likely pathogenic':
	CV = " AND ( B.clin_sig like '%Pathogenic%' or B.clin_sig like '%Likely_pathogenic%' ) and (B.clin_sig not like '%Conflict%') "
    else:
	CV = " AND ( "
	if cpath == 'path':
	    CV += " B.clin_sig like '%Pathogenic%' "

        if clpath == 'lpath' and CV == " AND ( ":
	    CV += " B.clin_sig like '%Likely_pathogenic%' "
	elif clpath == 'lpath':
	    CV += " or B.clin_sig like '%Likely_pathogenic%' "

        if cuncsig == 'uncsig' and CV == " AND ( ":
            CV += " B.clin_sig like '%Uncertain_significance%' "
        elif cuncsig == 'uncsig':
            CV += " or B.clin_sig like '%Uncertain_significance%' "

        if clbeni == 'lbeni' and CV == " AND ( ":
            CV += " B.clin_sig like '%Likely_benign%' "
        elif clbeni == 'lbeni':
            CV += " or B.clin_sig like '%Likely_benign%' "

        if cbeni == 'beni' and CV == " AND ( ":
            CV += " B.clin_sig like '%Benign%' "
        elif cbeni == 'beni':
            CV += " or B.clin_sig like '%Benign%' "

	CV += " ) and (B.clin_sig not like '%Conflict%') "

    stars0 = request.args.get('stars0')
    stars1 = request.args.get('stars1')
    stars2 = request.args.get('stars2')
    stars3 = request.args.get('stars3')
    stars4 = request.args.get('stars4')

    stars = ''

    if stars4 == "stars4":
        stars += '4,'
    if stars3 == "stars3":
        stars += '3,'
    if stars2 == "stars2":
        stars += '2,'
    if stars1 == "stars1":
        stars += '1,'
    if stars0 ==  "stars0":
        stars += '0,'
    if stars == "":
        stars = "4,3,2"
    if stars.endswith(','):
        stars = stars[:-1]

    starsql = ''
    if ',' in stars:
        starsql = " AND ("
        if '4' in stars:
            starsql += " B.Stars = 4 or "
        if '3' in stars:
            starsql += " B.Stars = 3 or "
        if '2' in stars:
            starsql += " B.Stars = 2 or "
        if '1' in stars:
            starsql += " B.Stars = 1 or "
        if '0' in stars:
            starsql += " B.Stars = 0 "
        if starsql.endswith(" or "):
            starsql = starsql[:-4]
        starsql += ") "

    if stars == '0':
        starsql = " AND B.Stars = 0 "
    if stars == '1':
        starsql = " AND B.Stars >= 1 "
    if stars == '2':
        starsql = " AND B.Stars >= 2 "
    if stars == '3':
        starsql = " AND B.Stars >= 3 "
    if stars == '4':
        starsql = " AND B.Stars = 4 "

    i_all = request.args.get('c_All')
    i_hig = request.args.get('c_hig')
    i_mod = request.args.get('c_mod')
    i_low = request.args.get('c_low')
    i_abl = request.args.get('c_ablation')
    i_acc = request.args.get('c_acceptor')
    i_don = request.args.get('c_donor')
    i_stg = request.args.get('c_stop-gained')
    i_fra = request.args.get('c_frameshift')
    i_stol = request.args.get('c_stop-lost')
    i_stal = request.args.get('c_start-lost')
    i_amp = request.args.get('c_amplification')
    i_ins = request.args.get('c_insertion')
    i_del = request.args.get('c_deletion')
    i_mis = request.args.get('c_missense')
    i_pro = request.args.get('c_prot-altering')
    i_reg = request.args.get('c_reg-region')
    i_spl = request.args.get('c_splice-region')
    i_inc = request.args.get('c_incomplete')
    i_sta = request.args.get('c_start-retained')
    i_sto = request.args.get('c_stop-retained')
    i_syn = request.args.get('c_synonymous')
    c_impact_listV2 = all_impactV2(i_all,i_hig,i_mod,i_low,i_abl,i_acc,i_don,i_stg,i_fra,i_stol,i_stal,i_amp,i_ins,i_del,i_mis,i_pro,i_reg,i_spl,i_inc,i_sta,i_sto,i_syn)
    c_impact3 = c_impact_listV2[0]
    c_impact2 = c_impact_listV2[1]

    c_ancestry = request.args.get('c_ancestry')

    if gt_anc == 'gt_anc':
        c_ancestry = pred_anc
    elif sr_anc == 'sr_anc' and upload_anc != 'none':
        c_ancestry = upload_anc
    c_ancestry_q = c_ancestry
    c_allele_f = request.args.get('c_af')
    c_allele_f2 = c_allele_f
    c_allele_q = c_allele_f
    c_af_anc = str(af_anc(c_allele_f,c_ancestry))

    if 'GNOMAD_' in c_af_anc:
        c_af_anc = c_af_anc.replace('GNOMAD_','A.GNOMAD_')
    if 'Consequence' in c_impact3:
        c_impact3 = c_impact3.replace('Consequence','A.Consequence')
    if 'impact' in c_impact3:
	c_impact3 = c_impact3.replace('impact', 'A.impact')

    stmt = ("select A.* , B.clin_id, B.clin_pheno, B.clin_db, B.clin_rev_stat, B.clin_sig, B.rsid, B.Stars from %s as A INNER JOIN gNOME_annotation_v2.clinvarV2 as B ON (A.CHROM = B.CHROM AND A.POS = B.POS AND A.REF = B.REF AND A.ALT = B.ALT) AND A.Symbol != '' AND A.Pick = 1 AND B.Assembly = 'GRCh37'  %s %s %s %s %s"%(genome_analysis_table, c_af_anc, CV, starsql,c_impact3,zygo))
    sys.stderr.write("[LOG] ClinVar: %s\n" % stmt)
    sys.stderr.flush()
    dicCLN = dictsV2(stmt,c_ancestry,'clin'," "," ")

    ts = time.time()
    clin_time = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S %f')
    sys.stderr.write("[LOG] ClinVar Time: %s\n" % clin_time)
    sys.stderr.flush()

        #########   #########   #########
        ###   ###   ###   ###   ###   ###
        ###   ###   #########   #########
        ###   ###   ###  ###    ###
        #########   ###   ###   ###

    o_zyg = request.args.get('o_zyg')
    o_hom = request.args.get('o_hom')
    o_het = request.args.get('o_het')

    zyg = zygosity(o_zyg,o_hom,o_het)
    zygo = zyg[0]
    o_zygo = zyg[1]

    i_all = request.args.get('o_All')
    i_hig = request.args.get('o_hig')
    i_mod = request.args.get('o_mod')
    i_low = request.args.get('o_low')
    i_abl = request.args.get('o_ablation')
    i_acc = request.args.get('o_acceptor')
    i_don = request.args.get('o_donor')
    i_stg = request.args.get('o_stop-gained')
    i_fra = request.args.get('o_frameshift')
    i_stol = request.args.get('o_stop-lost')
    i_stal = request.args.get('o_start-lost')
    i_amp = request.args.get('o_amplification')
    i_ins = request.args.get('o_insertion')
    i_del = request.args.get('o_deletion')
    i_mis = request.args.get('o_missense')
    i_pro = request.args.get('o_prot-altering')
    i_reg = request.args.get('o_reg-region')
    i_spl = request.args.get('o_splice-region')
    i_inc = request.args.get('o_incomplete')
    i_sta = request.args.get('o_start-retained')
    i_sto = request.args.get('o_stop-retained')
    i_syn = request.args.get('o_synonymous')
    o_impact_listV2 = all_impactV2(i_all,i_hig,i_mod,i_low,i_abl,i_acc,i_don,i_stg,i_fra,i_stol,i_stal,i_amp,i_ins,i_del,i_mis,i_pro,i_reg,i_spl,i_inc,i_sta,i_sto,i_syn)
    o_impact3 = o_impact_listV2[0]
    o_impact2 = o_impact_listV2[1]

    o_ancestry = request.args.get('o_ancestry')
    if gt_anc == 'gt_anc':
        o_ancestry = pred_anc
    elif sr_anc == 'sr_anc' and upload_anc != 'none':
        o_ancestry = upload_anc
    o_ancestry_q = o_ancestry
    o_allele_f = request.args.get('o_af')
    o_allele_f2 = o_allele_f
    o_allele_q = o_allele_f
    o_af_anc = str(af_anc(o_allele_f,o_ancestry))

    stmt = ("SELECT * from %s as A where A.Symbol != '' AND A.Pick = 1  AND A.Symbol in (select gene_symbol from  orphadata_best.orphanet) %s %s %s" % (genome_analysis_table, o_af_anc, o_impact3, zygo))
    sys.stderr.write("[LOG] Orpha: %s\n" % stmt)
    sys.stderr.flush()
    dicOrp = dictsV2(stmt,o_ancestry,'orp',orpdic2," ")
    ts = time.time()
    orp_time = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S %f')
    sys.stderr.write("[LOG] Orpha time: %s\n" % orp_time)
    sys.stderr.flush()

        ########        #########       #########
        ##     ##          ###          ###
        ##     ##          ###          #########
        ##     ##          ###                ###
        ########        #########       #########

    d_zyg = request.args.get('d_zyg')
    d_hom = request.args.get('d_hom')
    d_het = request.args.get('d_het')

    zyg = zygosity(d_zyg,d_hom,d_het)
    zygo = zyg[0]
    d_zygo = zyg[1]

    i_all = request.args.get('d_All')
    i_hig = request.args.get('d_hig')
    i_mod = request.args.get('d_mod')
    i_low = request.args.get('d_low')
    i_abl = request.args.get('d_ablation')
    i_acc = request.args.get('d_acceptor')
    i_don = request.args.get('d_donor')
    i_stg = request.args.get('d_stop-gained')
    i_fra = request.args.get('d_frameshift')
    i_stol = request.args.get('d_stop-lost')
    i_stal = request.args.get('d_start-lost')
    i_amp = request.args.get('d_amplification')
    i_ins = request.args.get('d_insertion')
    i_del = request.args.get('d_deletion')
    i_mis = request.args.get('d_missense')
    i_pro = request.args.get('d_prot-altering')
    i_reg = request.args.get('d_reg-region')
    i_spl = request.args.get('d_splice-region')
    i_inc = request.args.get('d_incomplete')
    i_sta = request.args.get('d_start-retained')
    i_sto = request.args.get('d_stop-retained')
    i_syn = request.args.get('d_synonymous')
    d_impact_listV2 = all_impactV2(i_all,i_hig,i_mod,i_low,i_abl,i_acc,i_don,i_stg,i_fra,i_stol,i_stal,i_amp,i_ins,i_del,i_mis,i_pro,i_reg,i_spl,i_inc,i_sta,i_sto,i_syn)
    d_impact3 = d_impact_listV2[0]
    d_impact2 = d_impact_listV2[1]

    d_ancestry = request.args.get('d_ancestry')
    if gt_anc == 'gt_anc':
        d_ancestry = pred_anc
    elif sr_anc == 'sr_anc' and upload_anc != 'none':
        d_ancestry = upload_anc
    d_ancestry_q = d_ancestry
    d_allele_f = request.args.get('d_af')
    d_allele_f2 = d_allele_f
    d_allele_q = d_allele_f
    d_af_anc = str(af_anc(d_allele_f,d_ancestry))

    pheno1 = request.args.get('pheno1')
    perc1 = request.args.get('perc1')
    perc3 = perc1
    if perc1 == None:
        perc1 = 1
    perc2 = perc1


    if pheno1 != "":
        pt = myFindZebra.get_genes_with_phenotype(pheno1,int(perc1))
        stmt=("SELECT * from %s as A where A.Symbol != '' AND A.Pick = 1 %s %s %s" % (genome_analysis_table, d_af_anc, d_impact3, zygo))
        dic20 = dictsV2(stmt,d_ancestry,'dis'," "," ")
    else:
        pt = ""
        dic20 = {}
    ts = time.time()
    dis_time = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S %f')

        ########    #####      ###   ###        #########
        ##    ##   ##         ## ## ## ##       ##
        ########   ##        ##   ###   ##      ##   ####
        ##    ##   ##       ##           ##     ##     ##   
        ##    ##    #####  ##             ##    #########

    a_zyg = request.args.get('a_zyg')
    a_hom = request.args.get('a_hom')
    a_het = request.args.get('a_het')

    zyg = zygosity(a_zyg,a_hom,a_het)
    zygo = zyg[0]
    a_zygo = zyg[1]

    i_all = request.args.get('a_All')
    i_hig = request.args.get('a_hig')
    i_mod = request.args.get('a_mod')
    i_low = request.args.get('a_low')
    i_abl = request.args.get('a_ablation')
    i_acc = request.args.get('a_acceptor')
    i_don = request.args.get('a_donor')
    i_stg = request.args.get('a_stop-gained')
    i_fra = request.args.get('a_frameshift')
    i_stol = request.args.get('a_stop-lost')
    i_stal = request.args.get('a_start-lost')
    i_amp = request.args.get('a_amplification')
    i_ins = request.args.get('a_insertion')
    i_del = request.args.get('a_deletion')
    i_mis = request.args.get('a_missense')
    i_pro = request.args.get('a_prot-altering')
    i_reg = request.args.get('a_reg-region')
    i_spl = request.args.get('a_splice-region')
    i_inc = request.args.get('a_incomplete')
    i_sta = request.args.get('a_start-retained')
    i_sto = request.args.get('a_stop-retained')
    i_syn = request.args.get('a_synonymous')
    a_impact_listV2 = all_impactV2(i_all,i_hig,i_mod,i_low,i_abl,i_acc,i_don,i_stg,i_fra,i_stol,i_stal,i_amp,i_ins,i_del,i_mis,i_pro,i_reg,i_spl,i_inc,i_sta,i_sto,i_syn)
    a_impact3 = a_impact_listV2[0]
    a_impact2 = a_impact_listV2[1]

    a_ancestry = request.args.get('a_ancestry')
    if gt_anc == 'gt_anc':
        a_ancestry = pred_anc
    elif sr_anc == 'sr_anc' and upload_anc != 'none':
        a_ancestry = upload_anc
    a_ancestry_q = a_ancestry
    a_allele_f = request.args.get('a_af')
    a_allele_f2 = a_allele_f
    a_allele_q = a_allele_f

    a_af_anc = str(af_anc(a_allele_f,a_ancestry))

    stmt = ("SELECT  A.*, B.pheno from %s as A inner join gNOME_annotation_v2.geneset_common as B on (gene_symbol = Symbol) where geneset_name = 'ACMG_SF_v2' AND A.Symbol != '' AND A.Pick = 1  %s %s %s"  % (genome_analysis_table, a_af_anc, a_impact3, zygo))

    sys.stderr.write("[LOG] ACMG: %s\n" % stmt)
    sys.stderr.flush()
    dic10 = dictsV2(stmt,a_ancestry,'acmg'," "," ")

    ts = time.time()
    acmg_time = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S %f')
    sys.stderr.write("[LOG] ACMG time: %s\n" % acmg_time)
    sys.stderr.flush()

        #########   #########   ###   ###
        ###   ###   ##           ### ###
        #########   ##   ####     #####
        ###         ##     ##    ### ###
        ###         #########   ###   ###

    p_zyg = request.args.get('p_zyg')
    p_hom = request.args.get('p_hom')
    p_het = request.args.get('p_het')

    zyg = zygosity(p_zyg,p_hom,p_het)
    zygo = zyg[0]
    p_zygo = zyg[1]

    i_all = request.args.get('p_All')
    i_hig = request.args.get('p_hig')
    i_mod = request.args.get('p_mod')
    i_low = request.args.get('p_low')
    i_abl = request.args.get('p_ablation')
    i_acc = request.args.get('p_acceptor')
    i_don = request.args.get('p_donor')
    i_stg = request.args.get('p_stop-gained')
    i_fra = request.args.get('p_frameshift')
    i_stol = request.args.get('p_stop-lost')
    i_stal = request.args.get('p_start-lost')
    i_amp = request.args.get('p_amplification')
    i_ins = request.args.get('p_insertion')
    i_del = request.args.get('p_deletion')
    i_mis = request.args.get('p_missense')
    i_pro = request.args.get('p_prot-altering')
    i_reg = request.args.get('p_reg-region')
    i_spl = request.args.get('p_splice-region')
    i_inc = request.args.get('p_incomplete')
    i_sta = request.args.get('p_start-retained')
    i_sto = request.args.get('p_stop-retained')
    i_syn = request.args.get('p_synonymous')
    p_impact_listV2 = all_impactV2(i_all,i_hig,i_mod,i_low,i_abl,i_acc,i_don,i_stg,i_fra,i_stol,i_stal,i_amp,i_ins,i_del,i_mis,i_pro,i_reg,i_spl,i_inc,i_sta,i_sto,i_syn)
    p_impact3 = p_impact_listV2[0]
    p_impact2 = p_impact_listV2[1]

    p_ancestry = request.args.get('p_ancestry')
    if gt_anc == 'gt_anc':
        p_ancestry = pred_anc
    elif sr_anc == 'sr_anc' and upload_anc != 'none':
        p_ancestry = upload_anc
    p_ancestry_q = p_ancestry
    p_allele_f = request.args.get('p_af')
    p_allele_f2 = p_allele_f
    p_allele_q = p_allele_f
    p_af_anc = str(af_anc(p_allele_f,p_ancestry))

    cpic = request.args.get('cpic')
    if cpic == 'cpic':
	cpic = 'cpic'
	cpic2 = " "
    else:
	cpic = 'not1'
	cpic2 = "AND hgmd_detail = 'nada'"

    vip = request.args.get('vip')
    if vip == 'vip':
        vip = 'vip'
        vip2 = " "
    else:
        vip = 'not1'
        vip2 = "AND hgmd_detail = 'nada'"

    pgrn = request.args.get('pgrn')
    if pgrn == 'pgrn':
        pgrn = 'pgrn'
        pgrn2 = " "
    else:
        pgrn = 'not1'
        pgrn2 = "AND hgmd_detail = 'nada'"

    cur = connect_user_db()

    stmt=("SELECT A.*,B.cpic_drug,B.guideline_id,'cpic' FROM %s as A INNER JOIN gNOME_annotation_v2.ref_cpic_variant as B ON (A.CHROM = B.Chromosome AND A.POS = B.pos AND A.REF = B.Reference_seq AND A.ALT = B.Variant_seq) WHERE A.Symbol != '' AND A.Pick = 1  %s %s" % (genome_analysis_table, cpic2, zygo))
    cur.execute(stmt)
    sys.stderr.write("[LOG] pgx1: %s\n" % stmt)
    sys.stderr.flush()
    cpicL = cur.fetchall()

    stmt=("SELECT A.*,id,'','vip' FROM %s as A INNER JOIN gNOME_annotation_v2.geneset_common as B ON A.Symbol = B.gene_symbol WHERE A.Symbol != '' AND A.Pick = 1 AND geneset_name = 'PGxVIP' %s %s %s %s" % (genome_analysis_table, p_af_anc, p_impact3, vip2, zygo))
    sys.stderr.write("[LOG] pgx2: %s\n" % stmt)
    sys.stderr.flush()
    cur.execute(stmt)
    cpicL += cur.fetchall()

    stmt=("SELECT A.*,'','','pgrn' FROM %s as A INNER JOIN gNOME_annotation_v2.geneset_common as B ON A.Symbol = B.gene_symbol WHERE A.Symbol != '' AND A.Pick = 1 AND geneset_name = 'PGRNseq' %s %s %s %s" % (genome_analysis_table, p_af_anc, p_impact3, pgrn2, zygo))
    sys.stderr.write("[LOG] pgx3: %s\n" % stmt)
    sys.stderr.flush()
    cur.execute(stmt)
    cpicL += cur.fetchall()

    dicVIP = {}
    stmt=("SELECT gene_symbol,id FROM gNOME_annotation_v2.geneset_common WHERE geneset_name='PGxVIP'")
    cur.execute(stmt)
    while(stmt is not None):
	dicVIP[stmt[0]] = stmt[1]
	stmt = cur.fetchone() 

    stmt=("SELECT gene_symbol FROM gNOME_annotation_v2.geneset_common WHERE geneset_name='PGRNseq'")
    cur.execute(stmt)
    pgrn_geneL = map(lambda x: x[0], cur.fetchall())

    dic30 = dictsV2(cpicL,p_ancestry,'pgx',dicVIP,pgrn_geneL)

    ts = time.time()
    pgx_time = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S %f')
    sys.stderr.write("[LOG] pgx time: %s\n" % pgx_time)
    sys.stderr.flush()

	#########   #########   ##     ##   #########
	##          ##          ####   ##   ##
	##  #####   #########   ## ### ##   #########
	##     ##   ##          ##   ####   ##
	#########   #########   ##     ##   #########

    genes1 = request.args.get('genes1')
    genes_q = genes1

    g_zyg = request.args.get('g_zyg')
    g_hom = request.args.get('g_hom')
    g_het = request.args.get('g_het')

    zyg = zygosity(g_zyg,g_hom,g_het)
    zygo = zyg[0]
    g_zygo = zyg[1]

    i_all = request.args.get('g_All')
    i_hig = request.args.get('g_hig')
    i_mod = request.args.get('g_mod')
    i_low = request.args.get('g_low')
    i_abl = request.args.get('g_ablation')
    i_acc = request.args.get('g_acceptor')
    i_don = request.args.get('g_donor')
    i_stg = request.args.get('g_stop-gained')
    i_fra = request.args.get('g_frameshift')
    i_stol = request.args.get('g_stop-lost')
    i_stal = request.args.get('g_start-lost')
    i_amp = request.args.get('g_amplification')
    i_ins = request.args.get('g_insertion')
    i_del = request.args.get('g_deletion')
    i_mis = request.args.get('g_missense')
    i_pro = request.args.get('g_prot-altering')
    i_reg = request.args.get('g_reg-region')
    i_spl = request.args.get('g_splice-region')
    i_inc = request.args.get('g_incomplete')
    i_sta = request.args.get('g_start-retained')
    i_sto = request.args.get('g_stop-retained')
    i_syn = request.args.get('g_synonymous')
    g_impact_listV2 = all_impactV2(i_all,i_hig,i_mod,i_low,i_abl,i_acc,i_don,i_stg,i_fra,i_stol,i_stal,i_amp,i_ins,i_del,i_mis,i_pro,i_reg,i_spl,i_inc,i_sta,i_sto,i_syn)
    g_impact3 = g_impact_listV2[0]
    g_impact2 = g_impact_listV2[1]

    g_ancestry = request.args.get('g_ancestry')
    if gt_anc == 'gt_anc':
        g_ancestry = pred_anc
    elif sr_anc == 'sr_anc' and upload_anc != 'none':
        g_ancestry = upload_anc
    g_ancestry_q = g_ancestry
    g_allele_f = request.args.get('g_af')
    g_allele_f2 = g_allele_f
    g_allele_q = g_allele_f
    g_af_anc = str(af_anc(g_allele_f,g_ancestry))

    genes1 = request.args.get('genes1')
    genes_q = genes1
    count4 = 0
    if genes_q == "":
        genes_q = "all"#none selected
        genes3 = ""
    elif ',' in genes1 or " " in genes1 or "<br>"in genes1:
        genes2 = genes1.replace(' ',',')
        genes2 = genes2.replace('<br>',',')
        genes2 = genes2.split(",")
        genes3 =" "
        genes3 += " AND (Symbol = \'"+genes2[0]+"\'" 
        count = 0
        for i in genes2:
           genes3 += " OR Symbol = \'"+genes2[count]+"\'"
           count +=1
           count4 += 1
        genes3 +=")"
    else:
        count4 = 1
        genes3 = " AND Symbol = \'" + genes1 + "\'"
    if genes1 != "":
        stmt=("SELECT * from %s as A where Symbol != '.' AND Symbol != '' AND Pick = 1 %s %s %s %s" % (genome_analysis_table, genes3, zygo, g_impact3, g_af_anc))
        dic40 = dictsV2(stmt,g_ancestry,'genes'," "," ")
    else:
	dic40 = {}

    ts = time.time()
    genes_time = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S %f')

        #########   #########   #########   #########
           ###      ###   ###      ###      ###   ###
           ###      #########      ###      ###   ###
           ###      ###  ###       ###      ###   ###
           ###      ###   ###   #########   #########

    t_zyg = request.args.get('t_zyg')
    t_hom = request.args.get('t_hom')
    t_het = request.args.get('t_het')

    zyg = zygosity(t_zyg,t_hom,t_het)
    zygo = zyg[0]
    t_zygo = zyg[1]

    if 'GT' in zygo:
        zygo = zygo.replace('GT','A.GT')

    i_all = request.args.get('t_All')
    i_hig = request.args.get('t_hig')
    i_mod = request.args.get('t_mod')
    i_low = request.args.get('t_low')
    i_abl = request.args.get('t_ablation')
    i_acc = request.args.get('t_acceptor')
    i_don = request.args.get('t_donor')
    i_stg = request.args.get('t_stop-gained')
    i_fra = request.args.get('t_frameshift')
    i_stol = request.args.get('t_stop-lost')
    i_stal = request.args.get('t_start-lost')
    i_amp = request.args.get('t_amplification')
    i_ins = request.args.get('t_insertion')
    i_del = request.args.get('t_deletion')
    i_mis = request.args.get('t_missense')
    i_pro = request.args.get('t_prot-altering')
    i_reg = request.args.get('t_reg-region')
    i_spl = request.args.get('t_splice-region')
    i_inc = request.args.get('t_incomplete')
    i_sta = request.args.get('t_start-retained')
    i_sto = request.args.get('t_stop-retained')
    i_syn = request.args.get('t_synonymous')
    t_impact_listV2 = all_impactV2(i_all,i_hig,i_mod,i_low,i_abl,i_acc,i_don,i_stg,i_fra,i_stol,i_stal,i_amp,i_ins,i_del,i_mis,i_pro,i_reg,i_spl,i_inc,i_sta,i_sto,i_syn)
    t_impact3 = t_impact_listV2[0]
    t_impact2 = t_impact_listV2[1]

    if 'Consequence' in t_impact3:
        t_impact3 = t_impact3.replace('Consequence','A.Consequence')
    if 'impact' in t_impact3:
	t_impact3 = t_impact3.replace('impact','A.impact')

    t_ancestry = request.args.get('t_ancestry')
    if gt_anc == 'gt_anc':
        t_ancestry = pred_anc
    elif sr_anc == 'sr_anc' and upload_anc != 'none':
        t_ancestry = upload_anc
    t_ancestry_q = t_ancestry
    t_allele_f = request.args.get('t_af')
    t_allele_f2 = t_allele_f
    t_allele_q = t_allele_f
    t_af_anc = str(af_anc(t_allele_f,t_ancestry))
    if 'GNOMAD_' in t_af_anc:
	t_af_anc = t_af_anc.replace('GNOMAD_','A.GNOMAD_')
   
    father = request.args.get('father')
    father_genome = "%s.%s" % (os.environ['CGAR_DB_USER'], father)
#    if father in sample_genomes:
#        father_genome = "test.guest_" + father + "_MA_v1_0_cds"
#    else:
#        father_genome = session['username'] + "_" + father + "_MA_v1_0_cds"

    mother = request.args.get('mother')
    mother_genome = "%s.%s" % (os.environ['CGAR_DB_USER'], mother)
#    if mother in sample_genomes:
#        mother_genome = "test.guest_" + mother + "_MA_v1_0_cds"
#    else:
#        mother_genome = session['username'] + "_" + mother + "_MA_v1_0_cds"

    denovodb = request.args.get('denovodb')

    if mother != 'no_mother' and father != 'no_father' and mother != raw_genome and father != raw_genome:
	stmt=("CREATE temporary TABLE ttt (SELECT A.* from %s as A LEFT JOIN %s as B ON (A.CHROM=B.CHROM AND A.POS=B.POS AND A.REF=B.REF AND A.ALT=B.ALT) where A.Symbol != '' AND A.Pick = 1 AND B.CHROM IS NULL AND B.POS IS NULL AND B.REF IS NULL AND B.ALT IS NULL %s %s %s)" % (genome_analysis_table, mother_genome, t_af_anc, t_impact3, zygo))

	stmt2=("select A.*,ifnull(study_name,'N/A'),ifnull(PubMedID,'N/A'),ifnull(Primary_phenotype,'N/A'),ifnull(transcriptID,'N/A'),ifnull(gene_symbol,'N/A'),ifnull(exonintron,'N/A') from (SELECT ttt.* from ttt LEFT JOIN %s as A ON (ttt.CHROM=A.CHROM AND ttt.POS=A.POS AND ttt.REF=A.REF AND ttt.ALT=A.ALT) where  ttt.Symbol != '' AND ttt.Pick = 1 AND A.CHROM IS NULL AND A.POS IS NULL AND A.REF IS NULL AND A.ALT IS NULL) as A left join gNOME_annotation_v2.denovoDB as B on (A.CHROM = B.CHROM AND A.POS = B.POS and A.REF = B.REF AND A.ALT = B.ALT)" % (father_genome))

	cur = connect_user_db()
	cur.execute(stmt)
        dicT = dictsV2(stmt2,t_ancestry,'trio'," "," ")
    else:
        dicT = {}
	if mother == "no_mother":
	    mother = "None Selected"
        if father == "no_father":
            father = "None Selected"

    ts = time.time()
    trio_time = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S %f')

        #########      #########      ####   ###
        ###            ##     ##      ### #  ###
        ###            #########      ### ## ### 
        ###            ##     ##      ###  # ###
        #########      ##     ##      ###   ####

    can_zyg = request.args.get('can_zyg')
    can_hom = request.args.get('can_hom')
    can_het = request.args.get('can_het')

    zyg = zygosity(can_zyg,can_hom,can_het)
    zygo = zyg[0]
    can_zygo = zyg[1]

    i_all = request.args.get('can_All')
    i_hig = request.args.get('can_hig')
    i_mod = request.args.get('can_mod')
    i_low = request.args.get('can_low')
    i_abl = request.args.get('can_ablation')
    i_acc = request.args.get('can_acceptor')
    i_don = request.args.get('can_donor')
    i_stg = request.args.get('can_stop-gained')
    i_fra = request.args.get('can_frameshift')
    i_stol = request.args.get('can_stop-lost')
    i_stal = request.args.get('can_start-lost')
    i_amp = request.args.get('can_amplification')
    i_ins = request.args.get('can_insertion')
    i_del = request.args.get('can_deletion')
    i_mis = request.args.get('can_missense')
    i_pro = request.args.get('can_prot-altering')
    i_reg = request.args.get('can_reg-region')
    i_spl = request.args.get('can_splice-region')
    i_inc = request.args.get('can_incomplete')
    i_sta = request.args.get('can_start-retained')
    i_sto = request.args.get('can_stop-retained')
    i_syn = request.args.get('can_synonymous')
    can_impact_listV2 = all_impactV2(i_all,i_hig,i_mod,i_low,i_abl,i_acc,i_don,i_stg,i_fra,i_stol,i_stal,i_amp,i_ins,i_del,i_mis,i_pro,i_reg,i_spl,i_inc,i_sta,i_sto,i_syn)
    can_impact3 = can_impact_listV2[0]
    can_impact2 = can_impact_listV2[1]

    can_ancestry = request.args.get('can_ancestry')
    if gt_anc == 'gt_anc':
        can_ancestry = pred_anc
    elif sr_anc == 'sr_anc' and upload_anc != 'none':
        can_ancestry = upload_anc
    can_ancestry_q = can_ancestry
    can_allele_f = request.args.get('can_af')
    can_allele_f2 = can_allele_f
    can_allele_q = can_allele_f
    can_af_anc = str(af_anc(can_allele_f,can_ancestry))
#    if len(can_af_anc.strip()) < 1:
#        can_af_anc = 'and true'

    can_cosmic = request.args.get('can_cosmic')
    can_census = request.args.get('can_census')

    cosmic = can_cosmic
    census = can_census
    if (cosmic == 'cosmic' and census == 'census') or (cosmic != "cosmic" and census != "census"):
	cosmic_1 = ' '
    elif cosmic == 'cosmic':
        cosmic_1 = ' AND cosmic_id is not null '
    elif census == 'census':
        cosmic_1 = ' '

    control = request.args.get('control')

    if control == 'no_control':
	stmt=("SELECT A.*,ifnull(cosmic_id,'No'),ifnull(num_samp_w_mut,'nada'),ifnull(tier,'No'),ifnull(hallmark,'No'),ifnull(tumour_type_s,'Unknown'),ifnull(tumour_type_g,'Unknown'),ifnull(cancer_syndrome,'nada'),ifnull(tissue_type,'nada'), ifnull(role_in_cancer,'nada'),ifnull(mutation_type,'nada') from %s as A left join gNOME_annotation_v2.cosmic as B on (A.CHROM = B.CHROM and A.POS = B.POS and A.REF = B.REF and A.ALT = B.ALT) left join gNOME_annotation_v2.census as C on (A.Symbol = C.gene_symbol) where A.Symbol != '' AND A.Pick = 1 AND tier = '1' AND assembly = 'GRCh37' %s %s %s %s"  % (genome_analysis_table, cosmic_1, can_af_anc, can_impact3, zygo))
        sys.stderr.write("[LOG] cancer: %s\n" % stmt)
        sys.stderr.flush()
    else:
#        if control in sample_genomes:
#            control_id = "guest_" + control + "_MA_v1_0_cds"
#            control_id = "test.guest_" + control + "_MA_v1_0_cds"
#        else:
#            control_id = session['username'] + "_" + control + "_MA_v1_0_cds"
        control_id = os.environ['CGAR_DB_USER'] + '.' + control
	stmt2=("CREATE temporary TABLE ccc (SELECT A.* from %s as A LEFT JOIN %s as B ON (A.CHROM = B.CHROM and A.POS = B.POS and A.REF = B.REF and A.ALT = B.ALT) where A.Symbol != '' AND A.Pick = 1 AND B.CHROM IS NULL AND B.POS IS NULL AND B.REF IS NULL AND B.ALT IS NULL AND B.Symbol IS NULL AND B.Consequence IS NULL)" % (genome_analysis_table, control_id))
        sys.stderr.write("[LOG] cancer1: %s\n" % stmt2)
        sys.stderr.flush()
        cur.execute(stmt2)
        stmt=("SELECT A.*,ifnull(cosmic_id,'No'),ifnull(num_samp_w_mut,'nada'),ifnull(tier,'No'),ifnull(hallmark,'No'),ifnull(tumour_type_s,'Unknown'),ifnull(tumour_type_g,'Unknown'),ifnull(cancer_syndrome,'nada'),ifnull(tissue_type,'nada'), ifnull(role_in_cancer,'nada'),ifnull(mutation_type,'nada') from ccc as A left join gNOME_annotation_v2.cosmic as B on (A.CHROM = B.CHROM and A.POS = B.POS and A.REF = B.REF and A.ALT = B.ALT) left join gNOME_annotation_v2.census as C on (A.Symbol = C.gene_symbol) where A.Symbol != '' AND A.Pick = 1 and tier = '1' %s %s %s %s"% (cosmic_1,can_af_anc,can_impact3,zygo))
        sys.stderr.write("[LOG] cancer2: %s\n" % stmt)
        sys.stderr.flush()

    dicCAN = dictsV2(stmt,can_ancestry,'can'," "," ")

    ts = time.time()
    cancer_time = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S %f')
    sys.stderr.write("[LOG] cancer time: %s\n" % cancer_time)
    sys.stderr.flush()

    h_reset = request.args.get('h_reset')
    c_reset = request.args.get('c_reset')
    o_reset = request.args.get('o_reset')
    d_reset = request.args.get('d_reset')
    a_reset = request.args.get('a_reset')
    p_reset = request.args.get('p_reset')
    g_reset = request.args.get('g_reset')
    t_reset = request.args.get('t_reset')
    can_reset = request.args.get('can_reset')

    all_reset = str(h_reset) + "|" + str(c_reset) + "|" + str(o_reset) + "|" + str(d_reset) + "|" + str(a_reset) + "|" + str(p_reset) + "|" + str(g_reset) + "|" + str(t_reset) + "|" + str(can_reset)
#    ts = time.time()
#    st2 = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')

    fil_sel2 = request.args.get('fil_sel')
    tab_sel2 = request.args.get('tab_sel')

    return render_template('report.html',priviledge=priviledge,dic1 = dic1,genome1 = raw_genome,h_allele_q=h_allele_q,h_ancestry_q=h_ancestry_q,rp1=rp1,h_zyg=h_zyg,h_hom=h_hom,h_het=h_het,h_zygo=h_zygo,h_ancestry=h_ancestry,h_allele_f=h_allele_f2,h_impact_listV2=h_impact_listV2,h_impact2=h_impact2,cv=cv,dmOnly=dmOnly,dmQue=dmQue,dmOther=dmOther,dic20=dic20,perc1=perc1,pheno1=pheno1,perc2=perc2,perc3=perc3,dicCLN=dicCLN,c_zyg=c_zyg,c_hom=c_hom,c_het=c_het,c_zygo=c_zygo,cv2=cv2,cpath=cpath,clpath=clpath,cuncsig=cuncsig,clbeni=clbeni,cbeni=cbeni,stars=stars,c_allele_q=c_allele_q,c_ancestry_q=c_ancestry_q,c_impact_listV2=c_impact_listV2,c_impact2=c_impact2,c_ancestry=c_ancestry,c_allele_f=c_allele_f2,dicOrp=dicOrp,o_zyg=o_zyg,o_hom=o_hom,o_het=o_het,o_zygo=o_zygo,o_allele_q=o_allele_q,o_ancestry_q=o_ancestry_q,o_ancestry=o_ancestry,o_allele_f=o_allele_f2,o_impact_listV2=o_impact_listV2,o_impact2=o_impact2,dic10=dic10,a_zyg=a_zyg,a_hom=a_hom,a_het=a_het,a_zygo=a_zygo,a_allele_q=a_allele_q,a_ancestry_q=a_ancestry_q,a_ancestry=a_ancestry,a_allele_f=a_allele_f2,a_impact_listV2=a_impact_listV2,a_impact2=a_impact2,d_zyg=d_zyg,d_hom=d_hom,d_het=d_het,d_zygo=d_zygo,d_allele_q=d_allele_q,d_ancestry_q=d_ancestry_q,d_ancestry=d_ancestry,d_allele_f=d_allele_f2,d_impact_listV2=d_impact_listV2,d_impact2=d_impact2,dic30=dic30,p_zyg=p_zyg,p_hom=p_hom,p_het=p_het,p_zygo=p_zygo,cpic=cpic,vip=vip,pgrn=pgrn,p_allele_q=p_allele_q,p_ancestry_q=p_ancestry_q,p_ancestry=p_ancestry,p_allele_f=p_allele_f2,p_impact_listV2=p_impact_listV2,p_impact2=p_impact2,dic40=dic40,g_zyg=g_zyg,g_hom=g_hom,g_het=g_het,g_zygo=g_zygo,g_allele_q=g_allele_q,g_ancestry_q=g_ancestry_q,g_ancestry=g_ancestry,g_allele_f=g_allele_f2,g_impact_listV2=g_impact_listV2,g_impact2=g_impact2,genes_q=genes_q,genes1=genes1,genes3=genes3,count4=count4,dicT = dicT,father=father,mother=mother,t_zyg=t_zyg,t_hom=t_hom,t_het=t_het,t_zygo=t_zygo,t_allele_q=t_allele_q,t_ancestry_q=t_ancestry_q,t_ancestry=t_ancestry,t_allele_f=t_allele_f2,t_impact_listV2=t_impact_listV2,t_impact2=t_impact2,dicCAN=dicCAN,can_cosmic=can_cosmic,can_census=can_census,can_zyg=can_zyg,can_hom=can_hom,can_het=can_het,can_zygo=can_zygo,can_allele_q=can_allele_q,can_ancestry_q=can_ancestry_q,can_ancestry=can_ancestry,can_allele_f=can_allele_f2,can_impact_listV2=can_impact_listV2,can_impact2=can_impact2,fil_sel2=fil_sel2,tab_sel2=tab_sel2,anc=main_anc,father_anc=father_anc,mother_anc=mother_anc,pred_anc=pred_anc,upload_anc=upload_anc,pt=pt,gt_anc=gt_anc,sr_anc=sr_anc,df_anc=df_anc,control=control,all_reset=all_reset,orpdic2=orpdic2)

def zygosity(zyg,hom,het):
    if zyg == 'zyg':
        zygoq = ''
        zygo = 'Any'
    elif hom == 'hom':
        zygoq = "and GT = 'homozygous' "
        zygo = 'Homozygous'
    elif het == 'het':
        zygoq = "and GT = 'heterozygous' "
        zygo = 'Heterozygous'
    else:
        zygoq = ''
        zygo = 'Any'
    zygol = []
    zygol.append(zygoq)
    zygol.append(zygo)
    return(zygol)

def af_anc(af,anc):
    if af == 'all' and anc == 'all':
        af = " "
    elif af == 'all' and anc != 'all':
        af = " "
    elif af == '<=5%' and anc == 'all':
        af = " AND GNOMAD_AFR_AF <= 0.05 AND GNOMAD_AMR_AF <= 0.05  AND GNOMAD_EAS_AF <= 0.05  AND GNOMAD_NFE_AF <= 0.05  AND GNOMAD_SAS_AF <= 0.05 "
    elif af == '<=3%' and anc == 'all':
        af = " AND GNOMAD_AFR_AF <= 0.03 AND GNOMAD_AMR_AF <= 0.03  AND GNOMAD_EAS_AF <= 0.03  AND GNOMAD_NFE_AF <= 0.03  AND GNOMAD_SAS_AF <= 0.03 "
    elif af == '<=1%' and anc == 'all':
        af = " AND GNOMAD_AFR_AF <= 0.01 AND GNOMAD_AMR_AF <= 0.01  AND GNOMAD_EAS_AF <= 0.01  AND GNOMAD_NFE_AF <= 0.01  AND GNOMAD_SAS_AF <= 0.01 "
    elif af == '<=0.5%' and anc == 'all':
        af = " AND GNOMAD_AFR_AF <= 0.005 AND GNOMAD_AMR_AF <= 0.005  AND GNOMAD_EAS_AF <= 0.005 AND GNOMAD_NFE_AF <= 0.005  AND GNOMAD_SAS_AF <= 0.005 "

    elif anc == 'afr' and af == '<=5%':
        af = " AND GNOMAD_AFR_AF <= 0.05 "
    elif anc == 'afr' and af == '<=3%':
        af = " AND GNOMAD_AFR_AF <= 0.03 "
    elif anc == 'afr' and af == '<=1%':
        af = " AND GNOMAD_AFR_AF <= 0.01 "
    elif anc == 'afr' and af == '<=0.5%':
        af = " AND GNOMAD_AFR_AF <= 0.005 "

    elif anc == 'amr' and af == '<=5%':
        af = " AND GNOMAD_AMR_AF <= 0.05 "
    elif anc == 'amr' and af == '<=3%':
        af = " AND GNOMAD_AMR_AF <= 0.03 "
    elif anc == 'amr' and af == '<=1%':
        af = " AND GNOMAD_AMR_AF <= 0.01 "
    elif anc == 'amr' and af == '<=0.5%':
        af = " AND GNOMAD_AMR_AF <= 0.005 "

    elif anc == 'eas' and af == '<=5%':
        af = " AND GNOMAD_EAS_AF <= 0.05 "
    elif anc == 'eas' and af == '<=3%':
        af = " AND GNOMAD_EAS_AF <= 0.03 "
    elif anc == 'eas' and af == '<=1%':
        af = " AND GNOMAD_EAS_AF <= 0.01 "
    elif anc == 'eas' and af == '<=0.5%':
        af = " AND GNOMAD_EAS_AF <= 0.005 "

    elif anc == 'eur' and af == '<=5%':
        af = " AND GNOMAD_NFE_AF <= 0.05 "
    elif anc == 'eur' and af == '<=3%':
        af = " AND GNOMAD_NFE_AF <= 0.03 "
    elif anc == 'eur' and af == '<=1%':
        af = " AND GNOMAD_NFE_AF <= 0.01 "
    elif anc == 'eur' and af == '<=0.5%':
        af = " AND GNOMAD_NFE_AF <= 0.005 "

    elif anc == 'sas' and af == '<=5%':
        af = " AND GNOMAD_SAS_AF <= 0.05 "
    elif anc == 'sas' and af == '<=3%':
        af = " AND GNOMAD_SAS_AF <= 0.03 "
    elif anc == 'sas' and af == '<=1%':
        af = " AND GNOMAD_SAS_AF <= 0.01 "
    elif anc == 'sas' and af == '<=0.5%':
        af = " AND GNOMAD_SAS_AF <= 0.005 "
    return(af)

def all_impactV2(i_all,i_hig,i_mod,i_low,i_abl,i_acc,i_don,i_stg,i_fra,i_stol,i_stal,i_amp,i_ins,i_del,i_mis,i_pro,i_reg,i_spl,i_inc,i_sta,i_sto,i_syn):
    i_ret = []
    impact3 = " "
    impact2 = "Any"
    if i_all == "all":
        impact3 = "AND (impact != 'MODIFIER')"
        #        impact3 = "AND (Consequence like '%transcript_ablation%' or Consequence like '%splice_acceptor_variant%' or Consequence like '%splice_donor_variant%' or Consequence like '%stop_gained%' or Consequence like '%frameshift_variant%' or Consequence like '%stop_lost%' or Consequence like '%start_lost%' or Consequence like '%transcript_aplification%' or Consequence like '%inframe_insertion%' or Consequence like '%inframe_deletion%' or Consequence like '%missense_variant%' or Consequence like '%protein_altering_variant%' or Consequence like '%regulatory_region_ablation%' or Consequence like '%splice_region_variant%' or Consequence like '%incomplete_terminal_codon_variant%' or Consequence like '%start_retained_variant%' or Consequence like '%stop_retained_variant%' or Consequence like '%synonymous_variant%')"
        i_ret.extend((impact3,impact2))
    else:
        impact3 = "and ("
        if i_hig == "hig":
            impact3 += "impact = 'HIGH'"
            #            impact3 += "Consequence like '%transcript_ablation%' or Consequence like '%splice_acceptor_variant%' or Consequence like '%splice_donor_variant%' or Consequence like '%stop_gained%' or Consequence like '%frameshift_variant%' or Consequence like '%stop_lost%' or Consequence like '%start_lost%' or Consequence like '%transcript_amplification%'"
            impact2 = "All high impact"

        if i_mod == "mod":
            if impact3 == "and (":
                impact3 += "impact = 'MODERATE'"
                #                impact3 += "Consequence like '%inframe_insertion%' or Consequence like '%inframe_deletion%' or Consequence like '%missense_variant%' or Consequence like '%protein_altering_variant%' or Consequence like '%regulatory_region_ablation%'"
                impact2 = "All moderate impact"
            else:
                impact3 += " or impact = 'MODERATE'"
                #                impact3 += " or Consequence like '%inframe_insertion%' or Consequence like '%inframe_deletion%' or Consequence like '%missense_variant%' or Consequence like '%protein_altering_variant%' or Consequence like '%regulatory_region_ablation%'"
                impact2 += ", All moderate impact" 

        if i_low == "low":
            if impact3 == "and (":
                impact3 += "impact = 'LOW'"
                #                impact3 += "Consequence like '%splice_region_variant%' or Consequence like '%incomplete_terminal_codon_variant%' or Consequence like '%start_retained_variant%' or Consequence like '%stop_retained_variant%' or Consequence like '%synonymous_variant%'"
                impact2 = "All low impact"
            else:
                impact3 += " or impact = 'LOW'"
                #                impact3 += " or Consequence like '%splice_region_variant%' or Consequence like '%incomplete_terminal_codon_variant%' or Consequence like '%start_retained_variant%' or Consequence like '%stop_retained_variant%' or Consequence like '%synonymous_variant%'"
                impact2 += ", All low impact" 

        if i_abl == "ablation":
            if 'HIGH' not in impact3 and impact3 == "and (":
                impact3 += "Consequence like '%transcript_ablation%' "
                impact2 = "Transcript ablation"
            elif 'HIGH' not in impact3:
                impact3 += "Consequence like '%transcript_ablation%' "
                impact2 += ", Transcript ablation"

        if i_acc == "acceptor":
            if 'HIGH' not in impact3 and impact3 == "and (":
                impact3 += "Consequence like '%splice_acceptor_variant%'"
                impact2 = "Splice acceptor"
            elif 'HIGH' not in impact3:
                impact3 += " or Consequence like '%splice_acceptor_variant%'"
                impact2 += ", Splice acceptor"

        if i_don == "donor":
            if 'HIGH' not in impact3 and impact3 == "and (":
                impact3 += "Consequence like '%splice_donor_variant%'"
                impact2 = "Splice donor"
            elif 'HIGH' not in impact3:
                impact3 += " or Consequence like '%splice_donor_variant%'"
                impact2 += ", Splice donor"

        if i_stg == "stop-gained":
            if 'HIGH' not in impact3 and impact3 == "and (":
                impact3 += "Consequence like '%stop_gained%'"
                impact2 = "Stop gained"
            elif 'HIGH' not in impact3:
                impact3 += " or Consequence like '%stop_gained%'"
                impact2 += ", Stop gained"

        if i_fra == "frameshift":
            if 'HIGH' not in impact3 and impact3 == "and (":
                impact3 += "Consequence like '%frameshift_variant%'"
                impact2 = "Frameshift"
            elif 'HIGH' not in impact3:
                impact3 += " or Consequence like '%frameshift_variant%'"
                impact2 += ", Frameshift"

        if i_stol == "stop-lost":
            if 'HIGH' not in impact3 and impact3 == "and (":
                impact3 += "Consequence like '%stop_lost%'"
                impact2 = "Stop lost"
            elif 'HIGH' not in impact3:
                impact3 += " or Consequence like '%stop_lost%'"
                impact2 += ", Stop lost"

        if i_stal == "start-lost":
            if 'HIGH' not in impact3 and impact3 == "and (":
                impact3 += "Consequence like '%start_lost%'"
                impact2 = "Start lost"
            elif 'HIGH' not in impact3:
                impact3 += " or Consequence like '%start_lost'"
                impact2 += ", Start lost"

        if i_amp == "amplification":
            if 'HIGH' not in impact3 and impact3 == "and (":
                impact3 += "Consequence like '%transcript_amplification%'"
                impact2 = "Transcript amplification"
            elif 'HIGH' not in impact3:
                impact3 += " or Consequence like '%transcript_amplification%'"
                impact2 += ", Transcript amplification"

        if i_ins == "insertion":
            if 'MODERATE' not in impact3 and impact3 == "and (":
                impact3 += "Consequence like '%inframe_insertion%'"
                impact2 = "Inframe insertion"
            elif 'MODERATE' not in impact3:
                impact3 += " or Consequence like '%inframe_insertion%'"
                impact2 += ", Inframe insertion"

        if i_del == "deletion":
            if 'MODERATE' not in impact3 and impact3 == "and (":
                impact3 += "Consequence like '%inframe_deletion%'"
                impact2 = "Inframe deletion"
            elif 'MODERATE' not in impact3:
                impact3 += " or Consequence like '%inframe_deletion%'"
                impact2 += ", Inframe deletion"

        if i_mis == "missense":
            if 'MODERATE' not in impact3 and impact3 == "and (":
                impact3 += "Consequence like '%missense_variant%'"
                impact2 = "Missense"
            elif 'MODERATE' not in impact3:
                impact3 += " or Consequence like '%missense_variant%'"
                impact2 += ", Missense"

        if i_pro == "prot-altering":
            if 'MODERATE' not in impact3 and impact3 == "and (":
                impact3 += "Consequence like '%protein_altering_variant%'"
                impact2 = "Protein altering"
            elif 'MODERATE' not in impact3:
                impact3 += " or Consequence like '%protein_altering_variant%'"
                impact2 += ", Protein altering"

        if i_reg == "reg-region":
            if 'MODERATE' not in impact3 and impact3 == "and (":
                impact3 += "Consequence like '%regulatory_region_ablation%'"
                impact2 = "Regulatory region ablation"
            elif 'MODERATE' not in impact3:
                impact3 += " or Consequence like '%regulatory_region_ablation%'"
                impact2 += ", Regulatory region ablation"

        if i_spl == "splice-region":
            if 'LOW' not in impact3 and impact3 == "and (":
                impact3 += "Consequence like '%splice_region_variant%'"
                impact2 = "Splice region"
            elif 'LOW' not in impact3:
                impact3 += " or Consequence like '%splice_region_variant%'"
                impact2 += ", Splice region"

        if i_inc == "incomplete":
            if 'LOW' not in impact3 and impact3 == "and (":
                impact3 += "Consequence like '%incomplete_terminal_codon_variant%'"
                impact2 = "Incomplete terminal codon"
            elif 'LOW' not in impact3:
                impact3 += " or Consequence like '%incomplete_terminal_codon_variant%'"
                impact2 += ", Incomplete terminal codon"

        if i_sta == "start-retained":
            if 'LOW' not in impact3 and impact3 == "and (":
                impact3 += "Consequence like '%start_retained_variant%'"
                impact2 = "Start retained"
            elif 'LOW' not in impact3:
                impact3 += " or Consequence like '%start_retained_variant%'"
                impact2 += ", Start retained"

        if i_sto == "stop-retained":
            if 'LOW' not in impact3 and impact3 == "and (":
                impact3 += "Consequence like '%stop_retained_variant%'"
                impact2 = "Stop retained"
            elif 'LOW' not in impact3:
                impact3 += " or Consequence like '%stop_retained_variant%'"
                impact2 += ", Stop retained"

        if i_syn == "synonymous":
            if 'LOW' not in impact3 and impact3 == "and (":
                impact3 += "Consequence like '%synonymous_variant%'"
                impact2 = "Synonymous"
            elif 'LOW' not in impact3:
                impact3 += " or Consequence like '%synonymous_variant%'"
                impact2 += ", Synonymous"

        impact3 += ")"

        if impact3 == "and ()":
            impact3 = "AND impact != 'MODIFIER'"
            #            impact3 = "AND (Consequence like '%transcript_ablation%' or Consequence like '%splice_acceptor_variant%' or Consequence like '%splice_donor_variant%' or Consequence like '%stop_gained%' or Consequence like '%frameshift_variant%' or Consequence like '%stop_lost%' or Consequence like '%start_lost%' or Consequence like '%transcript_aplification%' or Consequence like '%inframe_insertion%' or Consequence like '%inframe_deletion%' or Consequence like '%missense_variant%' or Consequence like '%protein_altering_variant%' or Consequence like '%regulatory_region_ablation%' or Consequence like '%splice_region_variant%' or Consequence like '%incomplete_terminal_codon_variant%' or Consequence like '%start_retained_variant%' or Consequence like '%stop_retained_variant%' or Consequence like '%synonymous_variant%')"
            impact2 = "Any"

        i_ret.extend((impact3,impact2))

    return(i_ret)

def dictsV2(stmt,ancestry,tab,extra1,extra2):
    if tab != "pgx":
        cur = connect_user_db()
        cur.execute(stmt)
        records = cur.fetchall()
    else:
        records = stmt
        keys = []
    dic = {}
    count = 0 
    for row in records:

        Symbol = row[13]
        pli_score = str(row[104])

	ch = row[24].split(':')[0]
	chrH = {'chr1':'NC_000001.10','chr2':'NC_000002.11','chr3':'NC_000003.11','chr4':'NC_000004.11','chr5':'NC_000005.9','chr6':'NC_000006.11','chr7':'NC_000007.13','chr8':'NC_000008.10','chr9':'NC_000009.11','chr10':'NC_000010.10',
		'chr11':'NC_000011.9','chr12':'NC_000012.11','chr13':'NC_000013.10','chr14':'NC_000014.8','chr15':'NC_000015.9','chr16':'NC_000016.9','chr17':'NC_000017.10','chr18':'NC_000018.9','chr19':'NC_000019.9','chr20':'NC_000020.10',
		'chr21':'NC_000021.8','chr22':'NC_000022.10','chrX':'NC_000023.10','chrY':'NC_000024.9',
		'1':'NC_000001.10','2':'NC_000002.11','3':'NC_000003.11','4':'NC_000004.11','5':'NC_000005.9','6':'NC_000006.11','7':'NC_000007.13','chr8':'NC_000008.10','9':'NC_000009.11','10':'NC_000010.10',
		'11':'NC_000011.9','12':'NC_000012.11','13':'NC_000013.10','14':'NC_000014.8','15':'NC_000015.9','16':'NC_000016.9','17':'NC_000017.10','18':'NC_000018.9','19':'NC_000019.9','20':'NC_000020.10',
		'21':'NC_000021.8','22':'NC_000022.10','X':'NC_000023.10','Y':'NC_000024.9'}
	hgvsg = row[24]
	if ch in chrH:
		hgvsg = row[24].replace(ch, chrH[ch])
        hgvs = row[23]+'&'+row[22]+'&'+hgvsg
	## print HGVS info with RefSeq, pick random Refseq transcript/protein that matches consequence type (among the same variant)
        if row[22].startswith("ENS"):
		cur = connect_user_db()
		gid = request.args.get('select_genome')
		cur.execute("SELECT count(*) FROM CGAR_web_user.%s WHERE CHROM='%s' AND POS='%s' AND REF='%s' AND ALT = '%s' AND IMPACT = '%s' AND Consequence = '%s' AND HGVSc not like 'ENS%%'" % (gid, row[0], row[1], row[2], row[3], row[12], row[11]))
		cnt = int(cur.fetchone()[0])
		if cnt > 0:
			cur.execute("SELECT HGVSc,HGVSp FROM CGAR_web_user.%s WHERE CHROM='%s' AND POS='%s' AND REF='%s' AND ALT = '%s' AND IMPACT = '%s' AND Consequence = '%s' AND HGVSc not like 'ENS%%' LIMIT 1" % (gid, row[0], row[1], row[2], row[3], row[12], row[11]))
			(new_hgvsc, new_hgvsp) = cur.fetchone()
			hgvs = new_hgvsp + '&' + new_hgvsc + '&' + hgvsg
        if '%3D' in hgvs:
            hgvs = hgvs.replace('%3D','=')

        if row[30] != '':
            hgmd1 = row[30].replace('_',' ')
            hgmd2 = ("%s %s" % (row[28],row[27])) 
        else:
            hgmd1 = 'N/A'
            hgmd2 = 'N/A'

        max_af = -10
        max_anc = ''

        GNOMAD_AFR_AF = row[47]
        GNOMAD_AMR_AF = row[48]
        GNOMAD_EAS_AF = row[50]
        GNOMAD_NFE_AF = row[52]
        GNOMAD_SAS_AF = row[54]

        if ancestry == 'all':
            if max_af < GNOMAD_AFR_AF:
                max_af = GNOMAD_AFR_AF
                max_anc = ' (AFR)'
            if max_af < GNOMAD_AMR_AF:
                max_af = GNOMAD_AMR_AF
                max_anc = ' (AMR)'
            if max_af < GNOMAD_EAS_AF:
                max_af = GNOMAD_EAS_AF
                max_anc = ' (EAS)'
            if max_af < GNOMAD_NFE_AF:
                max_af = GNOMAD_NFE_AF
                max_anc = ' (EUR)'
            if max_af < GNOMAD_SAS_AF:
                max_af = GNOMAD_SAS_AF
                max_anc = ' (SAS)'

        elif ancestry == 'afr':
            max_af = GNOMAD_AFR_AF
            max_anc = ' (AFR)'
        elif ancestry == 'amr':
            max_af = GNOMAD_AMR_AF
            max_anc = ' (AMR)'
        elif ancestry == 'eas':
            max_af = GNOMAD_EAS_AF
            max_anc = ' (EAS)'
        elif ancestry == 'eur':
            max_af = GNOMAD_NFE_AF
            max_anc = ' (EUR)'
        elif ancestry == 'sas':
            max_af = GNOMAD_SAS_AF
            max_anc = ' (SAS)'

        if max_af < 0:
            max_af = 'N/A'
        else:
            max_af = ("%.4f" % max_af)

        max_af = str(max_af) + max_anc
        
        tid = 'Transcript names'

        seq = row[24]

        link = ("%s-%s-%s-%s" % (row[0],str(row[1]),row[2],row[3]))

        if row[116] != '':
            cons = ("GERP++: %s"%str(row[116]))
        else:
            cons = "GERP++: N/A"

        gwas = row[37]

        path = ("Pathogenecity Prediction: SIFT: %s, PolyPhen2: %s"% (row[89],row[90]))

        AF = ("Allele Frequency in gnomAD (Genomes + Exomes):<br> %s (AFR), %s (AMR), %s (EAS), <a href='#' data-toggle='tooltip' title='Non-Finnish Europeans'>%s (EUR)</a>, %s (SAS)" % (GNOMAD_AFR_AF,GNOMAD_AMR_AF,GNOMAD_EAS_AF,GNOMAD_NFE_AF,GNOMAD_SAS_AF))

        genotype = row[5]

        coverage = row[114]
        if coverage >= 0 and coverage <= 1 :
            coverage = float(coverage)*100
        else:
            coverage = 0.0

        if coverage >= 99:
            color = 'green'
        elif coverage >= 90:
            color = 'blue'
        elif coverage >= 50:
            color = 'yellow'
        else:
            color = 'red'

        coverage = str(coverage) +"%"

        impact = row[11].replace('_',' ')

        if tab == 'clin':
            clin_id = str(row[117])
            clin_pheno = row[118]
            clin_db = row[119]
            clin_rev_stat = row[120].replace('_',' ')
            clin_sig = row[121].replace('_',' ')
	    rsid = str(row[122])
	    stars = str(row[123])
	    i = (hgvs+"||"+hgmd1+"||"+hgmd2+"||"+max_af+"||"+tid+"||"+seq+"||"+link+"||"+cons+"||"+gwas+"||"+path+"||"+AF+"||"+Symbol+"#"+pli_score+"||"+impact+"||"+clin_id+"||"+clin_pheno+"||"+clin_db+"||"+clin_rev_stat+"||"+clin_sig+"||"+genotype+"||"+coverage+"||"+color+"||"+stars)

        elif tab == 'orp':
            if Symbol in extra1:
                for orp2 in extra1[Symbol]:
                    orp2 = orp2.split('\t')
                    orphaid = str(orp2[2])
                    orpha_link = orp2[3]
                    age_onset = orp2[6]
                    age_death = orp2[7]
                    inheritance = orp2[8].strip()
                    if inheritance == ' ':
                        inheritance = 'No information'
                    prevalence = orp2[9].strip()
                    if prevalence == '|':
                        prevalence = 'No information'

                    hpo_disorder = orp2[10]

                    i = (hgvs+"|||"+hgmd1+"|||"+hgmd2+"|||"+max_af+"|||"+tid+"|||"+seq+"|||"+link+"|||"+cons+"|||"+gwas+"|||"+path+"|||"+AF+"|||"+Symbol+"#"+pli_score+"|||"+impact+"|||"+genotype+"|||"+coverage+"|||"+color+"|||"+orp2[1].decode('iso-8859-1').encode("ascii", "ignore")+"|||"+orphaid+"|||"+orpha_link+"|||"+age_onset+"|||"+age_death+"|||"+inheritance+"|||"+prevalence+"|||"+hpo_disorder)
                    dic[count] = i
                    count += 1

        elif tab == 'acmg':
	    allpheno = row[117] 
	    allpheno = allpheno.split('|')
	    pheno = allpheno[0]
	    omim = allpheno[1]
            i = (hgvs+"|"+hgmd1+"|"+hgmd2+"|"+max_af+"|"+tid+"|"+seq+"|"+link+"|"+cons+"|"+gwas+"|"+path+"|"+AF+"|"+Symbol+"#"+pli_score+"|"+impact+"|"+genotype+"|"+coverage+"|"+color+"|"+pheno+"|"+omim)

        elif tab == 'pgx':
            if link not in keys:
                keys.append(link)
            else:
                continue
            if row[119] == 'cpic':
                cpic2 = 'Yes'
                cpicD = row[117]
                cpicG = row[118]
            else:
                cpic2 = 'No'
                cpicD = ' '
                cpicG = ' '
            if Symbol in extra1:
                vip2 = 'Yes'
                vipID = extra1[Symbol]
            else:
                vip2 = 'No'
                vipID = " "
            if Symbol in extra2:
                pgrn2 = 'Yes'
            else:
                pgrn2 = 'No'
            i = (hgvs+"|"+hgmd1+"|"+hgmd2+"|"+max_af+"|"+tid+"|"+seq+"|"+link+"|"+cons+"|"+gwas+"|"+path+"|"+AF+"|"+Symbol+"#"+pli_score+"|"+impact+"|"+cpic2+"|"+vip2+"|"+cpicD+"|"+cpicG+"|"+pgrn2+"|"+vipID+"|"+genotype+"|"+coverage+"|"+color)

	elif tab == 'trio':
	    study_name = row[117]
            phenotype = row[118]
            i = (hgvs+"|"+hgmd1+"|"+hgmd2+"|"+max_af+"|"+tid+"|"+seq+"|"+link+"|"+cons+"|"+gwas+"|"+path+"|"+AF+"|"+Symbol+"#"+pli_score+"|"+impact+"|"+genotype+"|"+coverage+"|"+color+"|"+study_name+"|"+phenotype)

	elif tab == 'can':
            cosmic_id = row[117]
	    num_samp_w_mut = row[118]
            tier = row[119]
	    hallmark = row[120]
            tumour_type_s = row[121]
            tumour_type_g = row[122]
            cancer_syndrome = row[123]
 	    tissue_type = row[124]
	    role_in_cancer = row[125]
	    mutation_type = row[126]
            i = (hgvs+"||"+hgmd1+"||"+hgmd2+"||"+max_af+"||"+tid+"||"+seq+"||"+link+"||"+cons+"||"+gwas+"||"+path+"||"+AF+"||"+Symbol+"#"+pli_score+"||"+impact+"||"+genotype+"||"+coverage+"||"+color+"||"+cosmic_id+"||"+num_samp_w_mut+"||"+tier+"||"+hallmark+"||"+tumour_type_s+"||"+tumour_type_g+"||"+cancer_syndrome+"||"+tissue_type+"||"+role_in_cancer+"||"+mutation_type)

        else:
            i = (hgvs+"|"+hgmd1+"|"+hgmd2+"|"+max_af+"|"+tid+"|"+seq+"|"+link+"|"+cons+"|"+gwas+"|"+path+"|"+AF+"|"+Symbol+"#"+pli_score+"|"+impact+"|"+genotype+"|"+coverage+"|"+color)

	if tab != 'orp':
	    dic[count] = i
            count += 1
    return (dic)

@app.route('/detailed_view')
def detailed_view():

    values = request.args.get('values')
    genome = request.args.get('genome')
    chrom, pos, ref, alt = values.split('-')
    stmt = ("select * from %s.%s where CHROM = '%s' AND POS = '%s' AND REF = '%s' AND ALT = '%s' AND Symbol != '' AND Impact != 'MODIFIER' " % (os.environ['CGAR_DB_USER'], genome, chrom,pos,ref,alt))
#    stmt = ("select * from test.guest_Miller_MA_v1_0_cds where CHROM = '%s' AND POS = '%s' AND REF = '%s' AND ALT = '%s' AND Symbol != '' AND Impact != 'MODIFIER' " % (chrom,pos,ref,alt))

    cur = connect_user_db()
    cur.execute(stmt)
    records = cur.fetchall()
    if len(records) < 1: ## non-coding variants
        stmt = ("select * from %s.%s where CHROM = '%s' AND POS = '%s' AND REF = '%s' AND ALT = '%s' AND Consequence not like '%%stream_gene_variant%%'" % (os.environ['CGAR_DB_USER'], genome, chrom,pos,ref,alt))
        cur.execute(stmt)
        records = cur.fetchall()
    hgvsc = []
    hgvsp = []
    hgnc = []
    symb = []
    exacpli = []
    transcript = []
    consequence = []
    condel = []
    sift = []
    cadd = []
    polyphen = []
    fathmm = []
    mutassesor = []
    muttaster = []
    provean = []
    revel = []
    swissprot = []
    trembl = []
    uniparc = []
    hmmpanther = []
    gene3d = []
    superfamily = []
    prosite = []
    pfam = []
    for row in records:
        if row[22] == '':
            hgvsc.append('-')
        else:
            hgvsc.append(row[22])

        if row[23] == '':
            hgvsp.append('-')
        else:
            hgvsp.append(row[23].replace('%3D','='))

        if row[22] == '':
            transcript.append('-')
        else:
            transcript1 = row[22].split('.')
            transcript.append(transcript1[0])

        if row[15] == '':
            hgnc.append('-')
        else:
            hgnc.append(row[15])
        if row[13] == '':
            symb.append('-')
        else:
            symb.append(row[13])
        if row[104] == '':
            exacpli.append('-')
        else:
            exacpli.append(row[104])

        if row[11] == '':
            consequence.append('-')
        else:
            consequence.append(row[11])

        if row[91] == '':
            condel.append('-')
        else:
            condel.append(row[91])

        if row[89] == '':
            sift.append('-')
        else:
            sift.append(row[89])

        if row[92] == '':
            cadd.append('-')
        else:
            cadd.append(row[92])

        if row[90] == '':
            polyphen.append('-')
        else:
            polyphen.append(row[90])

        if row[94] == '':
            fathmm.append('-')
        else:
            fathmm.append(row[94])

        if row[96] == '':
            mutassesor.append('-')
        else:
            mutassesor.append(row[96])

        if row[98] == '':
            muttaster.append('-')
        else:
            muttaster.append(row[98])

        if row[100] == '':
            provean.append('-')
        else:
            provean.append(row[100])

        if row[102] == '':
            revel.append('-')
        else:
            revel.append(row[102])

        AF = [str(row[38]),str(row[39]),str(row[40]),str(row[41]),str(row[42]),str(row[43]),str(row[44]),str(row[45]),str(row[46]),str(row[47]),str(row[48]),str(row[49]),str(row[50]),str(row[51]),str(row[52]),str(row[53]),str(row[54])]
        AF = '|'.join(AF)

        conservation = [str(row[108]),str(row[109]),str(row[116])]
        conservation = '|'.join(conservation)

        splicing = [str(row[105]),str(row[106]),str(row[107])]
        splicing = '|'.join(splicing)

        domains = str(row[84]).split('&')
	domains_dict = {'hmmpanther':[], 'Gene3D':[], 'Superfamily':[], 'PROSITE':[],'Pfam':[]}
	for i in domains:
		for src in domains_dict:
			if src in i:
				domains_dict[ src ].append( ':'.join( i.split(':')[1:] ) )
	if len( domains_dict['hmmpanther'] ) > 0:
		hmmpanther = hmmpanther + domains_dict['hmmpanther']
	if len( domains_dict['Gene3D'] ) > 0:
		gene3d = gene3d + domains_dict['Gene3D']
	if len( domains_dict['Superfamily'] ) > 0:
		superfamily = superfamily + domains_dict['Superfamily']
	if len( domains_dict['PROSITE'] ) > 0:
		prosite = prosite + domains_dict['PROSITE']
	if len( domains_dict['Pfam'] ) > 0:
		pfam = pfam + domains_dict['Pfam']

	if row[19] != '':
		swissprot = swissprot + row[19].split('&')

	if row[20] != '':
		trembl = trembl + row[20].split('&')

	if row[21] != '':
		uniparc = uniparc + row[21].split('&')

        pubmed = row[37]
    #for row
    if len(swissprot) > 0:
	swissprot = '&'.join( list(set(swissprot)) )
    else:
	swissprot = '-'
    if len(trembl) > 0:
	trembl = '&'.join( list(set(trembl)) )
    else:
	trembl = '-'
    if len(uniparc) > 0:
	uniparc = '&'.join( list(set(uniparc)) )
    else:
	uniparc = '-'
    if len(hmmpanther) > 0:
	hmmpanther = '&'.join( list(set(hmmpanther)) )
    else:
	hmmpanther = '-'
    if len(gene3d) > 0:
	gene3d = '&'.join( list(set(gene3d)) )
    else:
	gene3d = '-'
    if len(superfamily) > 0:
	superfamily = '&'.join( list(set(superfamily)) )
    else:
	superfamily = '-'
    if len(prosite) > 0:
	prosite = '&'.join( list(set(prosite)) )
    else:
	prosite = '-'
    if len(pfam) > 0:
	pfam = '&'.join( list(set(pfam)) )
    else:
	pfam = '-'

    return render_template('detailed_view.html',hgnc=hgnc,symb=symb,exacpli=exacpli,transcript=transcript,hgvsc=hgvsc,hgvsp=hgvsp,consequence=consequence,condel=condel,sift=sift,cadd=cadd,polyphen=polyphen,fathmm=fathmm,mutassesor=mutassesor,muttaster=muttaster,provean=provean,revel=revel, AF=AF,conservation=conservation,splicing=splicing,swissprot=swissprot,trembl=trembl,uniparc=uniparc,hmmpanther=hmmpanther,gene3d=gene3d,superfamily=superfamily,prosite=prosite,pfam=pfam,pubmed=pubmed)

