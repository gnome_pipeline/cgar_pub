#!/bin/bash

# GNU Affero General Public License v3.0
#
# CGAR (Clinical Genome & Ancestry Report) - an interactive platform to identify
# phenotype-associated variants from next-generation sequencing data
# Copyright (C) 2017-2020  In-Hee Lee, Jose A Negron La Rosa
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

CGAR_HOME=/media/Data/cgar_pub
CACHE_DIR=/media/Data/VEP_cache
THREADS=30

vcf=$1
out=$2
build=$3

vep="/opt/vep/src/ensembl-vep/vep"

if [[ ("${build}" == "hg19") || ("${build}" == "b37") ]];then
	ref=/cache/Homo_sapiens.GRCh37.dna.primary_assembly.fa

	docker run -u www-data --rm -v ${CACHE_DIR}:/cache -v ${CGAR_HOME}:/work ensemblorg/ensembl-vep:release_94.5 \
		$vep --cache --fork ${THREADS} --species homo_sapiens --merged \
		--input_file ${vcf} \
		--format vcf \
		--output_file ${out} \
		--force_overwrite \
		--compress_output gzip \
		--stats_text \
		--assembly GRCh37 \
		--dir_cache /cache/ \
		--dir_plugins /cache/Plugins/ \
		--offline \
		--fasta ${ref} \
		--cache_version 94 \
		--buffer_size 10000 \
		--everything \
		--total_length \
		--allele_number \
		--hgvsg \
		--humdiv \
		--shift_hgvs 1 \
		--transcript_version \
		--canonical \
		--vcf \
		--flag_pick_allele \
		--plugin Downstream \
		--plugin TSSDistance \
		--plugin CSN \
		--plugin SpliceRegion \
		--plugin dbNSFP,/cache/dbNSFP/dbNSFP4.0b1.hg19.gz,Ensembl_proteinid,Uniprot_acc,MutationTaster_score,MutationTaster_pred,MutationAssessor_score,MutationAssessor_pred,FATHMM_score,FATHMM_pred,PROVEAN_score,PROVEAN_pred,REVEL_score \
		--plugin dbscSNV,/cache/dbscSNV/dbscSNV1.1_GRCh37.txt.gz \
		--plugin CADD,/cache/CADD/v1.4_201812/b37/whole_genome_SNVs.tsv.gz,/cache/CADD/v1.4_201812/b37/InDels.tsv.gz \
		--plugin Condel,/cache/Plugins/config/Condel/config/,b \
		--plugin LoFtool,/cache/LoFtool_scores.txt \
		--plugin ExACpLI,/cache/ExACpLI_values.txt \
		--custom /cache/hgmd/hgmd_pro_2018.2_hg19.vcf.gz,HGMD,vcf,exact,0,CLASS,MUT,PHEN,RANKSCORE \
		--custom /cache/gnomad/gnomad.genomes.r2.0.1.sites.noVEP.vcf.gz,GNOMAD_G,vcf,exact,0,AF_AFR,AF_AMR,AF_ASJ,AF_EAS,AF_FIN,AF_NFE,AF_OTH \
		--custom /cache/phastCons_100/hg19.100way.phastCons.bw,phastCons100,bigwig,overlap,0 \
		--custom /cache/phyloP_100/hg19.100way.phyloP100way.bw,phyloP100,bigwig,overlap,0 \
		--custom /cache/repeatmasker/rmsk.hg19.20181227.sorted.bed.gz,RMSK,bed,overlap,0 \
		--custom /cache/GERP/All_hg19_RS.bw,GERP,bigwig,overlap,0 \
		--custom /cache/gnomad/cov_gnomad_e_20x.txt.gz,COV_GNOMAD_E,bed,overlap,0 \
		--custom /cache/gnomad/cov_gnomad_g_20x.txt.gz,COV_GNOMAD_G,bed,overlap,0
fi

if [ "${build}" == "hg38" ];then
	ref=/cache/Homo_sapiens.GRCh38.dna.primary_assembly.fa 

	docker run -u www-data --rm -v ${CACHE_DIR}:/cache -v ${CGAR_HOME}:/work ensemblorg/ensembl-vep:release_94.5 \
		$vep --cache --fork ${THREADS} --species homo_sapiens --merged \
		--input_file ${vcf} \
		--format vcf \
		--output_file ${out} \
		--force_overwrite \
		--compress_output gzip \
		--stats_text \
		--assembly GRCh38 \
		--dir_cache /cache/ \
		--dir_plugins /cache/Plugins/ \
		--offline \
		--fasta ${ref} \
		--cache_version 94 \
		--buffer_size 10000 \
		--everything \
		--total_length \
	        --allele_number \
		--hgvsg \
		--humdiv \
		--shift_hgvs 1 \
		--transcript_version \
		--canonical \
		--vcf \
		--flag_pick_allele \
		--plugin Downstream \
		--plugin TSSDistance \
		--plugin CSN \
		--plugin SpliceRegion \
		--plugin dbNSFP,/cache/dbNSFP/dbNSFP4.0b1.hg38.gz,Ensembl_proteinid,Uniprot_acc,MutationTaster_score,MutationTaster_pred,MutationAssessor_score,MutationAssessor_pred,FATHMM_score,FATHMM_pred,PROVEAN_score,PROVEAN_pred,REVEL_score \
		--plugin dbscSNV,/cache/dbscSNV/dbscSNV1.1_GRCh38.txt.gz \
		--plugin CADD,/cache/CADD/v1.4_201812/b38/whole_genome_SNVs.tsv.gz,/cache/CADD/v1.4_201812/b38/InDels.tsv.gz \
		--plugin Condel,/cache/Plugins/config/Condel/config/,b \
		--plugin LoFtool,/cache/LoFtool_scores.txt \
		--plugin ExACpLI,/cache/ExACpLI_values.txt \
		--custom /cache/hgmd/hgmd_pro_2018.2_hg38.vcf.gz,HGMD,vcf,exact,0,CLASS,MUT,PHEN,RANKSCORE \
		--custom /cache/gnomad/gnomad.genomes.r2.0.1.sites.GRCh38.noVEP.vcf.gz,GNOMAD_G,vcf,exact,0,AF_AFR,AF_AMR,AF_ASJ,AF_EAS,AF_FIN,AF_NFE,AF_OTH \
		--custom /cache/phastCons_100/hg38.phastCons100way.bw,phastCons100,bigwig,overlap,0 \
		--custom /cache/phyloP_100/hg38.phyloP100way.bw,phyloP100,bigwig,overlap,0 \
		--custom /cache/repeatmasker/rmsk.hg38.20181227.sorted.bed.gz,RMSK,bed,overlap,0
fi
