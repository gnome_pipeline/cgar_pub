# GNU Affero General Public License v3.0
#
# CGAR (Clinical Genome & Ancestry Report) - an interactive platform to identify
# phenotype-associated variants from next-generation sequencing data
# Copyright (C) 2017-2020  In-Hee Lee, Jose A Negron La Rosa
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

class VariantSet:
	def __init__(self, row_list):
		self.varH = {}
		for row in row_list:
			key = (row[0], row[3], row[4], row[9], row[10])
	                gene_model = row[45]
        	        impact = row[46]
                	variant_seq_index = row[47]
	                gene_component = row[48]
        	        transcript_name_1 = row[49]
                	transcript_name_2 = row[50]
	                transcript_position = row[51]
        	        CDS_position = row[52]
                	protein_position = row[53]
	                reference_codon = row[54]
        	        variant_codon = row[55]
                	reference_amino_acid = row[56]
	                variant_amino_acid = row[57]
			
			if key not in self.varH:
				self.varH[ key ] = Variant(row)
			else:
				self.varH[ key] .addAnnot(gene_model, transcript_name_2, transcript_name_1, impact, gene_component, transcript_position, CDS_position, protein_position, reference_codon, variant_codon, reference_amino_acid, variant_amino_acid)

	def get_variants(self):
		return self.varH.values()

class Variant:
	def __init__(self, cols):
		self.chromosome = cols[0]
		self.source = cols[1]
		self.varType = cols[2]
		self.var_begin = cols[3]
		self.var_end = cols[4]
		self.score = cols[5]
		self.var_strand = cols[6]
		self.phase = cols[7]
		self.var_ID = cols[8]
		self.Reference_seq = cols[9]
		self.Variant_seq = cols[10]
		self.Genotype = cols[11]
		self.AF_1000G_AFR = cols[12]
		self.AF_1000G_ASN = cols[13]
		self.AF_1000G_EUR = cols[14]
		self.AF_200E_EUR = cols[15]
		self.AF_dbSNP132_AFR = cols[16]
		self.AF_dbSNP132_ASN = cols[17]
		self.AF_dbSNP132_EUR = cols[18]
		self.ID_dbSNP132_AFR = cols[19]
		self.ID_dbSNP132_ASN = cols[20]
		self.ID_dbSNP132_EUR = cols[21]
		self.allele_frequency_ESP5400_AA_detail = cols[22]
		self.allele_frequency_ESP5400_ALL_detail = cols[23]
		self.allele_frequency_ESP5400_EA_detail = cols[24]
		self.conserved_TFBS_avg_value = cols[25]
		self.Conserved_TFBS = cols[26]
		self.conserved_TFBS_overlap = cols[27]
		self.encode_regulation_detail = cols[28]
		self.encode_regulation_overlap = cols[29]
		self.miRNA = cols[30]
		self.SIFT_score = cols[31]
		self.SIFT_pred = cols[32]
		self.PPH2_score = cols[33]
		self.PPH2_pred = cols[34]
		self.Condel_score = cols[35]
		self.Condel_pred = cols[36]
		self.Average_conservation_score = cols[37]
		self.Sequence_repeat_detail = cols[38]
		self.Portion_with_sequence_repeat = cols[39]
		self.uniprot_seq_feature_detail = cols[40]
		self.uniprot_seq_feature_overlap = cols[41]
		self.hgmd_detail = cols[42]
		self.SafeGene = cols[43]
		self.gwas_catalog_detail = cols[44]
		gene_model = cols[45]
		impact = cols[46]
		variant_seq_index = cols[47]
		gene_component = cols[48]
		transcript_name_1 = cols[49]
		transcript_name_2 = cols[50]
		transcript_position = cols[51]
		CDS_position = cols[52]
		protein_position = cols[53]
		reference_codon = cols[54]
		variant_codon = cols[55]
		reference_amino_acid = cols[56]
		variant_amino_acid = cols[57]

		self.annotH = {}
		self.annotH = { 'refGene':[], 'ensGene':[], 'knownGene':[], 'ccdsGene':[] }
		self.addAnnot(gene_model, transcript_name_2, transcript_name_1, impact, gene_component, transcript_position, CDS_position, protein_position, reference_codon, variant_codon, reference_amino_acid, variant_amino_acid)

	def addAnnot(self, gene_model, transcript_name_2, transcript_name_1, impact, gene_component, transcript_position, CDS_position, protein_position, reference_codon, variant_codon, reference_amino_acid, variant_amino_acid):
		self.annotH[ gene_model ].append( Annotation(transcript_name_2, transcript_name_1, impact, gene_component, transcript_position, CDS_position, protein_position, reference_codon, variant_codon, reference_amino_acid, variant_amino_acid) )

	def get_annotations(self, gene_model):
		return self.annotH[ gene_model ]

	def get_uniq_annotations(self, gene_model):
		outH = {}
		for ann in self.annotH[ gene_model ]:
			key = (ann.transcript_name_2, ann.impact, ann.protein_position, ann.reference_amino_acid, ann.variant_amino_acid)
			if key not in outH:
				outH[ key ] = [ann.transcript_name_1]
			else:
				outH[ key ].append(ann.transcript_name_1)

		return outH

	def get_most_severe_annotations(self, gene_model):
		highL = []
		modL = []
		lowL = []
		for ann in self.annotH[ gene_model ]:
			if ann.impact in ['disrupt','frameshift','misstart','nonsense','nonstop']:
				highL.append(ann)
			elif ann.impact in ['in-frame-deletion','in-frame-insertion','missense']:
				modL.append(ann)
			else:
				lowL.append(ann)

		if len(highL) > 0: #has high impact
			return highL
		elif len(modL) > 0:
			return modL
		elif len(lowL) > 0:
			return lowL
		

class Annotation:
	def __init__(self, transcript_name_2, transcript_name_1, impact, gene_component, transcript_position, CDS_position, protein_position, reference_codon, variant_codon, reference_amino_acid, variant_amino_acid):
		self.transcript_name_2 = transcript_name_2
		self.transcript_name_1 = transcript_name_1
		self.impact = impact
		self.gene_component = gene_component
		self.transcript_position = transcript_position
		self.CDS_position = CDS_position
		self.protein_position = protein_position
		self.reference_codon = reference_codon
		self.variant_codon = variant_codon
		self.reference_amino_acid = reference_amino_acid
		self.variant_amino_acid = variant_amino_acid

#if __name__ == '__main__':

#	import MySQLdb
#	import sys, os

#	db = MySQLdb.connect(user="gNOME", db="gNOME_web_user", passwd="gNOMEweb")
#	cur = db.cursor()
#        cur.execute("SELECT * FROM mclean_DN_MA_v1_0_cds where hgmd_detail != ''")

#	Qvs = VariantSet(cur.fetchall())
#	dic1 = {}
#	for var in vs.get_variants():
#		annH = var.get_uniq_annotations('refGene')
#		count = 0
#		for ann in annH:
#			(transcript_name_2, impact, protein_position, reference_amino_acid, variant_amino_acid) = ann
#			sys.stdout.write(transcript_name_2)
#			if protein_position != '':
#				sys.stdout.write("\tp.%s%s%s" % (reference_amino_acid, protein_position, variant_amino_acid))
#			else:
#				sys.stdout.write("\t")
#			sys.stdout.write("\t%s" % impact)
#
#			if var.hgmd_detail != '':
#				hgmdL = var.hgmd_detail.split('|')
#				sys.stdout.write("\t%s\t%s %s" % (hgmdL[0], hgmdL[1], hgmdL[2]))
#			else:
#				sys.stdout.write("\t")
#
#			max_af = -10
#			if max_af < var.AF_1000G_AFR:
#				max_af = var.AF_1000G_AFR
#			if max_af < var.AF_1000G_ASN:
#				max_af = var.AF_1000G_ASN
#			if max_af < var.AF_1000G_EUR:
#				max_af = var.AF_1000G_EUR

#			if max_af < 0:
#				sys.stdout.write("\tN/A")
#			else:
#				sys.stdout.write("\t%s" % max_af)
#			if len(annH[ ann ]) > 1:
#				sys.stdout.write("[ %s ]" % ','.join(annH[ann]))
#			else:
#				sys.stdout.write("\t[ %s ]" % annH[ann][0])
#			sys.stdout.write("\n")
#			i = ["a"+str(impact)]
#			dic1[count] = i
#			count += 1
#	sys.stdout.write(dic1)
	#		print "%s:%s-%s %s>%s" % (var.chromosome, var.var_begin, var.var_end, var.Reference_seq, var.Variant_seq), transcript_name_2, "["+(','.join(annH[ ann ]))+"]", impact, reference_amino_acid, protein_position, variant_amino_acid

class acmg1:
        def __init__(self, row_list):
		self.gene_symbol = row[0]
		self.pheno = row[1]

class pgx1:
        def __init__(self, row_list):
                self.gene_symbol = row[0]

class extra_g:
        def __init__(self, row_list):
                self.transcript_name_2 = row[0]
		self.impact = row[1]
		self.protein_position = row[2]
		self.reference_amino_acid = row[3]
		self.variant_amino_acid = row[4]		

class ancestry:
        def __init__(self, row_list):
                self.AFR = row[0]
                self.AMR = row[1]
                self.EAS = row[2]
                self.EUR = row[3]
                self.SAS = row[4]

