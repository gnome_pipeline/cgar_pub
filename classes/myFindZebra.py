# GNU Affero General Public License v3.0
#
# CGAR (Clinical Genome & Ancestry Report) - an interactive platform to identify
# phenotype-associated variants from next-generation sequencing data
# Copyright (C) 2017-2020  In-Hee Lee, Jose A Negron La Rosa
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import urllib, json
import requests
import math

class myFindZebra:
    @staticmethod
    def get_genes_with_phenotype(keywords, perc):
	if int(perc) <= 0:
            return([])
        if int(perc) >= 100:
            perc = 100.0

        termL = map(lambda x: urllib.quote(x), keywords.strip().split(' '))
        termS = '+'.join(termL)

        url="http://www.findzebra.com/api/call/json/query?q=%s&fl=associated_gene,source" % termS
        response = requests.get(url)
        res = response.json()['response']
        nrow = int(res['numFound'])

        if nrow < 1:
            return([])

        url = "http://www.findzebra.com/api/call/json/query?q=%s&fl=associated_gene,source&rows=%s" % (termS, nrow)
        response = requests.get(url)
        arr = response.json()['response']['docs']
        scores = []
        genes = []
        for i in range(nrow):
            tmpH = arr[i]
            if tmpH['source'] != 'OMIM':
                continue
            if 'associated_gene' in tmpH:
                tmpL = tmpH['associated_gene']
                for item in tmpL:
                    g = item.split(':')[0]
                    score = float(item.split(':')[1])
                    genes.append(g)
                    scores.append(score)
                #for
            #if
        #for

        scores_sorted = sorted(scores, reverse=True)
        th = int(math.ceil( len(scores) * float(perc) / 100.0))
        th = max(0, th)
        th = min( len(scores)-1, th )
        th = scores_sorted[ th ]
        genes_final = []
        for i in range(len(scores)):
            if scores[i] >= th:
                genes_final.append(genes[i])
        tmpS = set(genes_final)
        return(list(tmpS))

    def get_genes_with_phenotype_count(keywords, count):
        if count <= 0:
            return([])

        if count > 1000:
            count = 1000

        termL = map(lambda x: urllib.quote(x), keywords.strip().split(' '))
        termS = '+'.join(termL)

        url="http://www.findzebra.com/api/call/json/query?q=%s&fl=associated_gene,source" % termS
        response = requests.get(url)
        res = response.json()['response']
        nrow = int(res['numFound'])

        if nrow < 1:
            return([])

        url = "http://www.findzebra.com/api/call/json/query?q=%s&fl=associated_gene,source&rows=%s" % (termS, nrow)
        response = requests.get(url)
        arr = response.json()['response']['docs']
        scores = []
        genes = []
        for i in range(nrow):
            tmpH = arr[i]
            if tmpH['source'] != 'OMIM':
                continue
            if 'associated_gene' in tmpH:
                tmpL = tmpH['associated_gene']
                for item in tmpL:
                    g = item.split(':')[0]
                    score = float(item.split(':')[1])
                    genes.append(g)
                    scores.append(score)
                #for
            #if
        #for

	if len(scores) <= count: ## return all
		tmpS = set(genes)
		return(list(tmpS))
	else:
		scores_sorted = sorted(scores, reverse=True)
		th = scores_sorted[count]
		genes_final = []
		for i in range(len(scores)):
			if scores[i] >= th:
				genes_final.append(genes[i])
		tmpS = set(genes_final)
		return(list(tmpS))



if __name__ == '__main__':
#    print myFindZebra.get_genes_with_phenotype('cold headache', 0.01)
    import re
    prog = re.compile('^([0-9A-Za-z]+)$')
    for gene in myFindZebra.get_genes_with_phenotype('cancer', 50):
            print gene
