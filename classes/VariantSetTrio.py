# GNU Affero General Public License v3.0
#
# CGAR (Clinical Genome & Ancestry Report) - an interactive platform to identify
# phenotype-associated variants from next-generation sequencing data
# Copyright (C) 2017-2020  In-Hee Lee, Jose A Negron La Rosa
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

class VariantSet2:
	def __init__(self, row_list):
		inFile = open('/media/gnome/gNOME/gnome_report_janegron/classes/canonical_transcript.hg19.20171024.txt','r')
		canonH = {'knownGene':{}, 'refGene':{}, 'ensGene':{}, 'ccdsGene':{}}
		inFile.readline()
		for line in inFile:
			colL = line.strip().split('\t')
			ucsc_id = colL[0]
			refseq_id = colL[1]
			ensembl_id = colL[2]
			ccds_id = colL[3]

			canonH['knownGene'][ ucsc_id ] = True
			if refseq_id != 'NA':
				canonH['refGene'][ refseq_id ] = True
			if ensembl_id != 'NA':
				canonH['ensGene'][ ensembl_id ] = True
			if ccds_id != 'NA':
				canonH['ccdsGene'][ ccds_id ] = True

		self.varH = {}
		for row in row_list:
			key = (row[0], row[3], row[4], row[9], row[10])
	                gene_model = row[45]
        	        impact = row[46]
                	variant_seq_index = row[47]
	                gene_component = row[48]
        	        transcript_name_1 = row[49]
                	transcript_name_2 = row[50]
	                transcript_position = row[51]
        	        CDS_position = row[52]
                	protein_position = row[53]
	                reference_codon = row[54]
        	        variant_codon = row[55]
                	reference_amino_acid = row[56]
	                variant_amino_acid = row[57]
			
			if key not in self.varH:
				self.varH[ key ] = Variant2(row)
			else:
				self.varH[ key] .addAnnot(gene_model, transcript_name_2, transcript_name_1, impact, gene_component, transcript_position, CDS_position, protein_position, reference_codon, variant_codon, reference_amino_acid, variant_amino_acid)

			if transcript_name_1 in canonH[ gene_model ]:
				self.varH[ key ].mark_canonical(transcript_name_1, gene_model)

		#for row

	def get_variants(self):
		return self.varH.values()

	def get_variants(self):
		return self.varH.values()

class Variant2:
	def __init__(self, cols, vip=False):
		self.chromosome = cols[0]
		self.source = cols[1]
		self.varType = cols[2]
		self.var_begin = cols[3]
		self.var_end = cols[4]
		self.score = cols[5]
		self.var_strand = cols[6]
		self.phase = cols[7]
		self.var_ID = cols[8]
		self.Reference_seq = cols[9]
		self.Variant_seq = cols[10]
		self.Genotype = cols[11]
		self.AF_1000G_AFR = cols[12]
		self.AF_1000G_ASN = cols[13]
		self.AF_1000G_EUR = cols[14]
		self.AF_200E_EUR = cols[15]
		self.AF_dbSNP132_AFR = cols[16]
		self.AF_dbSNP132_ASN = cols[17]
		self.AF_dbSNP132_EUR = cols[18]
		self.ID_dbSNP132_AFR = cols[19]
		self.ID_dbSNP132_ASN = cols[20]
		self.ID_dbSNP132_EUR = cols[21]
		self.allele_frequency_ESP5400_AA_detail = cols[22]
		self.allele_frequency_ESP5400_ALL_detail = cols[23]
		self.allele_frequency_ESP5400_EA_detail = cols[24]
		self.conserved_TFBS_avg_value = cols[25]
		self.Conserved_TFBS = cols[26]
		self.conserved_TFBS_overlap = cols[27]
		self.encode_regulation_detail = cols[28]
		self.encode_regulation_overlap = cols[29]
		self.miRNA = cols[30]
		self.SIFT_score = cols[31]
		self.SIFT_pred = cols[32]
		self.PPH2_score = cols[33]
		self.PPH2_pred = cols[34]
		self.Condel_score = cols[35]
		self.Condel_pred = cols[36]
		self.Average_conservation_score = cols[37]
		self.Sequence_repeat_detail = cols[38]
		self.Portion_with_sequence_repeat = cols[39]
		self.uniprot_seq_feature_detail = cols[40]
		self.uniprot_seq_feature_overlap = cols[41]
		self.hgmd_detail = cols[42]
		self.SafeGene = cols[43]
		self.gwas_catalog_detail = cols[44]
		gene_model = cols[45]
		impact = cols[46]
		variant_seq_index = cols[47]
		gene_component = cols[48]
		transcript_name_1 = cols[49]
		transcript_name_2 = cols[50]
		transcript_position = cols[51]
		CDS_position = cols[52]
		protein_position = cols[53]
		reference_codon = cols[54]
		variant_codon = cols[55]
		reference_amino_acid = cols[56]
		variant_amino_acid = cols[57]
                GNOMAD_E_AC_AFR = cols[58]
                GNOMAD_E_AC_AMR = cols[59]
                GNOMAD_E_AC_EAS = cols[60]
                GNOMAD_E_AC_FIN = cols[61]
                GNOMAD_E_AC_NFE = cols[62]
                GNOMAD_E_AC_SAS = cols[63]
                GNOMAD_E_Hom_AFR = cols[64]
                GNOMAD_E_Hom_AMR = cols[65]
                GNOMAD_E_Hom_EAS = cols[66]
                GNOMAD_E_Hom_FIN = cols[67]
                GNOMAD_E_Hom_NFE = cols[68]
                GNOMAD_E_Hom_SAS = cols[69]
                GNOMAD_E_AF_AFR = cols[70]
                GNOMAD_E_AF_AMR = cols[71]
                GNOMAD_E_AF_EAS = cols[72]
                GNOMAD_E_AF_FIN = cols[73]
                GNOMAD_E_AF_NFE = cols[74]
                GNOMAD_E_AF_SAS = cols[75]
                GNOMAD_E_AN_AFR = cols[76]
                GNOMAD_E_AN_AMR = cols[77]
                GNOMAD_E_AN_EAS = cols[78]
                GNOMAD_E_AN_FIN = cols[79]
                GNOMAD_E_AN_NFE = cols[80]
                GNOMAD_E_AN_SAS = cols[81]
                GNOMAD_AC_AFR = cols[82]
                GNOMAD_AC_AMR = cols[83]
                GNOMAD_AC_EAS = cols[84]
                GNOMAD_AC_FIN = cols[85]
                GNOMAD_AC_NFE = cols[86]
                GNOMAD_AC_SAS = cols[87]
                GNOMAD_Hom_AFR = cols[88]
                GNOMAD_Hom_AMR = cols[89]
                GNOMAD_Hom_EAS = cols[90]
                GNOMAD_Hom_FIN = cols[91]
                GNOMAD_Hom_NFE = cols[92]
                GNOMAD_Hom_SAS = cols[93]
                self.GNOMAD_AF_AFR = cols[94]
                self.GNOMAD_AF_AMR = cols[95]
                self.GNOMAD_AF_EAS = cols[96]
                GNOMAD_AF_FIN = cols[97]
                self.GNOMAD_AF_NFE = cols[98]
                self.GNOMAD_AF_SAS = cols[99]
                GNOMAD_AN_AFR = cols[100]
                GNOMAD_AN_AMR = cols[101]
                GNOMAD_AN_EAS = cols[102]
                GNOMAD_AN_FIN = cols[103]
                GNOMAD_AN_NFE = cols[104]
                GNOMAD_AN_SAS = cols[105]
                self.COV_GNOMAD_E =  cols[106]
                COV_GNOMAD_G =  cols[107]




		self.annotH = {}
		self.annotH = { 'refGene':[], 'ensGene':[], 'knownGene':[], 'ccdsGene':[] }
		self.addAnnot(gene_model, transcript_name_2, transcript_name_1, impact, gene_component, transcript_position, CDS_position, protein_position, reference_codon, variant_codon, reference_amino_acid, variant_amino_acid)

	def addAnnot(self, gene_model, transcript_name_2, transcript_name_1, impact, gene_component, transcript_position, CDS_position, protein_position, reference_codon, variant_codon, reference_amino_acid, variant_amino_acid):
		for annot in self.annotH[ gene_model ]:
			if annot.transcript_name_1 == transcript_name_1:
				return
		self.annotH[ gene_model ].append( Annotation(transcript_name_2, transcript_name_1, impact, gene_component, transcript_position, CDS_position, protein_position, reference_codon, variant_codon, reference_amino_acid, variant_amino_acid) )

	def get_annotations(self, gene_model):
		return self.annotH[ gene_model ]

	def get_uniq_annotations(self, gene_model):
		outH = {}
		for ann in self.annotH[ gene_model ]:
			key = (ann.transcript_name_2, ann.impact, ann.protein_position, ann.reference_amino_acid, ann.variant_amino_acid)
			if key not in outH:
				outH[ key ] = [ann.transcript_name_1]
			else:
				outH[ key ].append(ann.transcript_name_1)

		return outH

	def get_uniq_annotations_v2(self, gene_model):
		canonL = self.get_canonical_annotations(gene_model)
		if len(canonL) > 0:
			outH = {}
			for ann in canonL:
				key = (ann.transcript_name_2, ann.impact, ann.protein_position, ann.reference_amino_acid, ann.variant_amino_acid)
				if key not in outH:
					outH[ key ] = [ann.transcript_name_1]
				else:
					outH[ key ].append(ann.transcript_name_1)
			return outH

		severeL = self.get_most_severe_annotations(gene_model)
		if len(severeL) > 0:
			outH = {}
			for ann in severeL:
				key = (ann.transcript_name_2, ann.impact, ann.protein_position, ann.reference_amino_acid, ann.variant_amino_acid)
				if key not in outH:
					outH[ key ] = [ann.transcript_name_1]
				else:
					outH[ key ].append(ann.transcript_name_1)
			return outH

	def get_most_severe_annotations(self, gene_model):
		highL = []
		modL = []
		lowL = []
		for ann in self.annotH[ gene_model ]:
			if ann.impact in ['disrupt','frameshift','misstart','nonsense','nonstop']:
				highL.append(ann)
			elif ann.impact in ['in-frame-deletion','in-frame-insertion','missense']:
				modL.append(ann)
			else:
				lowL.append(ann)

		if len(highL) > 0: #has high impact
			return highL
		elif len(modL) > 0:
			return modL
		elif len(lowL) > 0:
			return lowL

	def get_canonical_annotations(self, gene_model):
		resL = []
		for ann in self.annotH[ gene_model ]:
			if ann.is_canonical():
				resL.append(ann)
		return resL

	def mark_canonical(self, transcript_name_1, gene_model):
		for annot in self.annotH[ gene_model ]:
			if annot.transcript_name_1 == transcript_name_1:
				annot.set_canonical()

class Annotation:
	def __init__(self, transcript_name_2, transcript_name_1, impact, gene_component, transcript_position, CDS_position, protein_position, reference_codon, variant_codon, reference_amino_acid, variant_amino_acid):
		self.transcript_name_2 = transcript_name_2
		self.transcript_name_1 = transcript_name_1
		self.impact = impact
		self.gene_component = gene_component
		self.transcript_position = transcript_position
		self.CDS_position = CDS_position
		self.protein_position = protein_position
		self.reference_codon = reference_codon
		self.variant_codon = variant_codon
		self.reference_amino_acid = reference_amino_acid
		self.variant_amino_acid = variant_amino_acid
		self.canonical = False

	def set_canonical(self):
		self.canonical = True

	def is_canonical(self):
		return self.canonical

if __name__ == '__main__':

	import MySQLdb
	import sys, os

