# GNU Affero General Public License v3.0
#
# CGAR (Clinical Genome & Ancestry Report) - an interactive platform to identify
# phenotype-associated variants from next-generation sequencing data
# Copyright (C) 2017-2020  In-Hee Lee, Jose A Negron La Rosa
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

class Variant:
    """ A variant with member variables corresponding to columns in sql database."""
    
    def __init__(self, query_row):
        #self.transcript_name_1 = query_row[0]
        self.transcript_name_2 = query_row[0]
        #self.varType = query_row[2]
        self.chromosome = query_row[1]
        self.var_begin = query_row[2]
        self.var_end = query_row[3]
        self.Reference_seq = query_row[4]
        self.Variant_seq = query_row[5]
        self.Genotype = query_row[6]
        self.AF_1000G_EUR = query_row[7]
        self.AF_1000G_ASN = query_row[8]
        self.AF_1000G_AFR = query_row[9]
        self.allele_frequency_ESP5400_ALL_detail = query_row[10]
        self.allele_frequency_ESP5400_EA_detail = query_row[11]
        self.allele_frequency_ESP5400_AA_detail = query_row[12]
        self.impact = query_row[13]
        self.Average_conservation_score = query_row[14]
	self.SIFT_score = query_row[16]
        self.SIFT_pred = query_row[17]
	self.PPH2_score = query_row[18]
        self.PPH2_pred = query_row[19]
	self.Condel_score = query_row[20]
        self.Condel_pred = query_row[21]
	self.pheno = query_row[22]
        if query_row[15] == '':
          self.hgmd_detail = ''
          self.hgmd2 = ''
          self.hgmd3 = ''
        else:
          self.hgmd_detail = query_row[15].split('|')[0]
          self.hgmd2 = query_row[15].split('|')[1]
          self.hgmd3 = query_row[15].split('|')[2]
	self.gwas_catalog_detail = query_row[22].split('|')[0]
        #self.transcript_name_1 = query_row[23]
        self.gene_model = query_row[23]
	self.reference_amino_acid = query_row[24]
	self.protein_position = query_row[25]
	self.variant_amino_acid = query_row[26]
        if len(query_row[22].split('|')) > 1:
	       self.gwas_catalog_detail2 = str(query_row[22].split('|')[1])
        else:
               self.gwas_catalog_detail2 = ""





	self.detailed_view = str(query_row[1])+":"+str(query_row[2])+"-"+str(query_row[3])+" "+str(query_row[4])+">"+str(query_row[5])+"\nAllele Frequency 1000G\nAFR: "+str(query_row[7])+"\nASN: "+str(query_row[8])+"\nEUR: "+str(query_row[9])+"\nMissense Prediction\nSIFT: " + str(query_row[16]) +"   "+str(query_row[17])+"\nPPH2: "+str(query_row[18]) + "   "+str(query_row[19])+"/nCondel: "+str(query_row[20])+"   "+str(query_row[21])

class acmg1:
    """ A variant with member variables corresponding to columns in sql database."""

    def __init__(self, query_row):
	self.gene_symbol = query_row[0]

